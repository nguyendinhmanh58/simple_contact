#ifndef DLLIST_H
#define DLLIST_H
#include "./../typedef.h"
/////////////////////////////////
void printfDllistNode(DllistElementtype e);
int funcDllist(DllistElementtype e1,DllistElementtype e2); // for SEARCH
int sosanhDllist(DllistNode *node1, DllistNode *node2); // for SORT
/////////////////////////////////
DllistNode* makeNodeDllist(DllistElementtype e);
DllistType* makeDllist();
void traverseDllist(DllistType* li);
void reverseDllist(DllistType* li);

int numberNodeDllist(DllistType* li);
bool hasPositionDllist(DllistType* li,int n);//Mang bat dau tu 1->n
DllistNode* findAtPositionDllist(DllistType* li,int n);//Mang bat dau tu 1->n

void insertAfterDllist(DllistType *li,DllistElementtype e, DllistNode* Node);
void insertAtBeginningDllist(DllistType* li, DllistElementtype e);
void insertAtEndDllist(DllistType *li,DllistElementtype e);
void insertBeforeDllist(DllistType *li,DllistElementtype e,DllistNode* Node);
void insertAtPositionDlllist(DllistType* li,DllistElementtype e,int n);

void deleteAtBeginningDllist(DllistType* li);
void deleteAtEndDllist(DllistType* li);
void deleteAtPositionDllist(DllistType* li,int n);
void deleteNodeDllist(DllistType *li,DllistNode* node);
void deleteListDllist(DllistType* li);

DllistNode* searchDllist(DllistType* li,DllistElementtype e);
void updateDllist(DllistNode* node,DllistElementtype e);
void swapDllist(DllistNode *n1,DllistNode *n2);
void quicksortDllist(DllistType* li);
void quicksort2Dllist(DllistNode **A, long l, long r);

#endif // DLLIST_H
