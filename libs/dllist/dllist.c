#include "dllist.h"
DllistNode* makeNodeDllist(DllistElementtype e)
{
    DllistNode *NewNode;
    NewNode = (DllistNode *)malloc(sizeof(DllistNode));
    if (NewNode == NULL)
    {
      //printf("Khong cap phat duoc bo nho\n");
      return NULL;
    }
    NewNode->element = e;
    NewNode->prev = NULL;
    NewNode->next = NULL;
    return NewNode;
}

DllistType* makeDllist()
{
    DllistType* li;
    li = (DllistType *)malloc(sizeof(DllistType));
    if(li==NULL)
    {
        //printf("Khong cap phat duoc bo nho cho List\n");
        return NULL;
    }
    else
    {
        li->head = NULL;
        li->tail = NULL;
        return li;
    }
}

void traverseDllist(DllistType* li)
{
    DllistNode * NewNode;
    if(li->head==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(NewNode = li->head; NewNode!=NULL; NewNode = NewNode->next)
    {
        printfDllistNode(NewNode->element);
    }
}

void reverseDllist(DllistType* li)
{
    DllistNode * NewNode;
    if(li->tail==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(NewNode = li->tail; NewNode!=NULL; NewNode = NewNode->prev)
    {
        printfDllistNode(NewNode->element);
    }
}

int numberNodeDllist(DllistType* li)
{
    DllistNode* Node;
    if(li == NULL) return 0;
    int i = 0;
    for (Node = li->head; Node != NULL; Node = Node->next) i++;
    return i;
}

bool hasPositionDllist(DllistType* li,int i)
{
    int n;
    n = numberNodeDllist(li);
    if(n==0) return false;
    if(i <= 0 || i > n) return false;
    return true;
}

DllistNode* findAtPositionDllist(DllistType* li,int n)
{
    DllistNode* Node;
    if (hasPositionDllist(li, n) == false)
    {
      //printf("Khong ton tai phan tu %d trong list\n",n);
      return NULL;
    }
    int i = 0;
    for (Node=li->head; Node != NULL; Node=Node->next)
    {
      i++;
      if (i == n) break;
      //i++;
    }
    return Node;
}

void insertAtBeginningDllist(DllistType *li,DllistElementtype e)
{
    DllistNode *NewNode;
    NewNode = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = NewNode;
        li->tail = NewNode;
        return;
    }
    if ((li->head)->prev != NULL)
    {
      printf("Con tro head cua list sai\n");
      return;
    }
    NewNode->next = li->head;
    li->head->prev = NewNode;
    li->head = NewNode;
}


void insertAtEndDllist(DllistType *li,DllistElementtype e)
{
    DllistNode *NewNode;
    NewNode = makeNodeDllist(e);
    if (li->head == NULL)
    {
        li->head = NewNode;
        li->tail = NewNode;
        return;
    }
    if ((li->tail)->next != NULL)
    {
      printf("Con tro tail cua list sai\n");
      return;
    }
    li->tail->next = NewNode;
    NewNode->prev = li->tail;
    li->tail = NewNode;
}

void insertAfterDllist(DllistType *li,DllistElementtype e, DllistNode* Node)
{
    DllistNode *NewNode;
    NewNode = makeNodeDllist(e);
    if(Node!=NULL)
    {
      if(Node== li->tail) li->tail = NewNode;
      NewNode->next = Node->next;
      NewNode->prev = Node;
      Node->next = NewNode;
      if (NewNode->next != NULL) NewNode->next->prev = NewNode;
    }
}

void insertBeforeDllist(DllistType *li,DllistElementtype e,DllistNode* Node)
{
    DllistNode *NewNode;
    NewNode = makeNodeDllist(e);
    if(Node!=NULL)
    {
        if(Node == li->head) li->head = NewNode;
        NewNode->next = Node;
        NewNode->prev = Node->prev;
        Node->prev = NewNode;
        if (NewNode->prev != NULL) NewNode->prev->next = NewNode;
    }

}

void insertAtPositionDlllist(DllistType* li,DllistElementtype e,int n)
{
    DllistNode* Node = findAtPositionDllist(li, n);
    if (Node == NULL)
    {
        printf("insertatposition is error\n");
        return;
    }
    insertBeforeDllist(li,e,Node);
}

void deleteAtBeginningDllist(DllistType* li)
{
    if (li->head == NULL) return;
    if ((li->head)->prev != NULL)
    {
        printf("Con tro head cua list sai\n");
        exit(0);
    }
    DllistNode *temp;
    temp = li->head;
    li->head = (li->head)->next;
    if (li->head != NULL) (li->head)->prev = NULL;
    if (li->head == NULL) li->tail = NULL;
    free(temp);
}

void deleteAtEndDllist(DllistType* li)
{
    if (li->tail == NULL) return;
    if ((li->tail)->next != NULL)
    {
        printf("Con tro tail cua list sai\n");
        exit(0);
    }
    DllistNode *temp;
    temp = (li->tail)->prev;
    if (temp != NULL) temp->next = NULL;
    free(li->tail);
    li->tail = temp;
    if (li->tail == NULL) li->head = NULL;
}

void deleteNodeDllist(DllistType *li,DllistNode* node)
{
    if (node == NULL) return;
    if (node == li->head) deleteAtBeginningDllist(li);
    else
    if (node == li->tail) deleteAtEndDllist(li);
    else
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
        free(node);
    }
}

void deleteAtPositionDllist(DllistType* li,int n)
{
    DllistNode* Node = findAtPositionDllist(li, n);
    if (Node == NULL)
    {
      printf("deleteatposition is error\n");
      return;
    }
    deleteNodeDllist(li,Node);
}




void deleteListDllist(DllistType* li)
{
    while(li->head!=NULL)
        deleteAtBeginningDllist(li);
}

DllistNode* searchDllist(DllistType* li,DllistElementtype e)
{
    DllistNode* Node;
    Node = li->head;
    while (Node != NULL)
    {
      if(funcDllist(Node->element,e)==0) break;
      Node = Node->next;
    }

    return Node;
}

void updateDllist(DllistNode* node,DllistElementtype e)
{
    node->element = e;
}


void swapDllist(DllistNode *n1,DllistNode *n2)
{
  DllistElementtype a;
  a = n1->element;
  n1->element = n2->element;
  n2->element = a;
}

void quicksortDllist(DllistType* li)
{
  DllistNode* Node;
  long n, i = 0;
  DllistNode **A;
  n = numberNodeDllist(li);
  A = (DllistNode **)malloc(n * sizeof(DllistNode*));
  for (Node = li->head; Node != NULL; Node = Node->next)
    {
      A[i] = Node;
      i++;
    }
  quicksort2Dllist(A, 0, i-1);
}

void quicksort2Dllist(DllistNode **A, long l, long r)
{
  long i, j;
  DllistNode *p;
  if (l < r)
    {
      p = A[l]; i = l; j = r;
      while (i < j)
	{
	  i++;
	  while (i < r && sosanhDllist(A[i], p) < 0) i++;
	  while (j > l && sosanhDllist(A[j], p) > 0) j--;
	  swapDllist(A[i], A[j]);
	}
      swapDllist(A[i], A[j]); swapDllist(A[l], A[j]);
      quicksort2Dllist(A,l,j-1);
      quicksort2Dllist(A,j+1,r);
    }
}

void printfDllistNode(DllistElementtype e){
    printf("%s|%s\n", e.name, e.desc);
}
int funcDllist(DllistElementtype e1,DllistElementtype e2){
    // FOR SEARCH
    return strcmp(e1.name, e2.name);
}
int sosanhDllist(DllistNode *node1, DllistNode *node2){
    return strcmp(node1->element.name, node2->element.name);
}
