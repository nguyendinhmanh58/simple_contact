#ifndef BTREE_H
#define BTREE_H
#include "./../typedef.h"
#define COMPARE_BY_NAME 1
#define COMPARE_BY_FAVORITE 2

////
int insCompare(bTreeElementtype e1, bTreeElementtype e2);
int searchCompare(bTreeElementtype e1,bTreeElementtype e2);
int searchMultiCompare(bTreeElementtype e1,bTreeElementtype e2);
int delCompare(bTreeElementtype e1,bTreeElementtype e2);
void printElement(bTreeElementtype e);
///
void makeTree(TreeType *tree);
TreeType leftTree(TreeType root);
TreeType rightTree(TreeType root);
bool isLeaf(TreeNode* node);
bool isEmptyTree(TreeType root);
TreeNode* makeTreeNode(bTreeElementtype e);
int numberNodeTree(TreeType root);
int heightOfTree(TreeType root);
int numberOfLeaf(TreeType root);
int numberOfInternalNode(TreeType root);
TreeType creatFrom2(TreeType l,TreeType r,bTreeElementtype e);
TreeNode* addLeft(TreeType *root,bTreeElementtype e);
TreeNode* addRight(TreeType *root,bTreeElementtype e);
int numberOfRightChildren(TreeType root);
void bInsert(TreeType *root,bTreeElementtype e);
void inOrderPrint(TreeType root);
void preOrderPrint(TreeType root);
TreeNode *bSearch(TreeType root,bTreeElementtype e);
void bSearchMulti(TreeType root,bTreeElementtype e, TreeNode* listContactFounded[], int* n);
void bSearchContactsInGroup(TreeType root,char* group_name, TreeNode* listContactFounded[], int* n);
void bSearchFavoriteContacts(TreeType root, TreeNode* listContactFounded[], int* n);
TreeNode *findMin(TreeType root);
TreeNode *findMax(TreeType root);
void deleteBtreeNode(TreeType* root,bTreeElementtype e);
void deleteTree(TreeType* root);

#endif
