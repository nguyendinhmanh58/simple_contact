#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "sllib.h"
///////////////////////////////////////
SllistNode* makeNodeSllist(elementtype element)
{
    SllistNode *Node;
    Node = (SllistNode *)malloc(sizeof(SllistNode));
    if(Node==NULL)
    {
        printf("Khong cap phat duoc bo nho\n");
        return NULL;
    }
    Node->element = element;
    Node->next = NULL;
    return Node;
}


SllistType* makeSllist()
{
    SllistType *li;
    li = (SllistType *)malloc(sizeof(SllistType));
    if(li==NULL)
    {
        printf("Loi. Khong cap phat duoc bo nho de tao Single Link List\n");
        return NULL;
    }
    else
    {
        li->root = NULL;
        return li;
    }

}

void traverseSllist(SllistType* li)
{
    SllistNode * Node;
    if(li->root==NULL)
    {
        printf("Danh sach trong!\n");
    }else
    for(Node = li->root; Node!=NULL; Node = Node->next)
    {
        printfSllistNode(Node->element);
    }
}

int numberNodeSllist(SllistType* li)
{
    int n=0;
    SllistNode* node;
    if(li->root==NULL) return 0;
    node = li->root;
    while(node!=NULL)
    {
        n++;
        node = node->next;
    }
    return n;
}

bool hasPositionSllist(SllistType* li,int i)
{
    int n = numberNodeSllist(li);
    if(n==0) return false;
    if(i<=0 || i>n) return false;
    return true;
}

SllistNode* findAtPositionSllist(SllistType* li,int n)
{
    SllistNode* Node;
    if(hasPositionSllist(li,n))
    {
        int i = 0;
        for (Node=li->root; Node != NULL; Node=Node->next)
        {
            i++;
            if (i == n) break;
        }
        return Node;
    }
    else
        return NULL;
}

SllistNode* prevNode(SllistType* li,SllistNode* node)
{
    SllistNode* prev = li->root;
    SllistNode* Node = li->root;
    while(Node!=NULL && Node!= node)
    {
        prev = Node;
        Node = Node->next;
    }
    if(Node==NULL) prev = NULL;
    return prev;
}

bool isTailNodeSllist(SllistType* li,SllistNode* node)
{
    SllistNode* Node = li->root;
    while((Node->next)!=NULL)
    {
        Node = Node->next;
    }
    if(Node==node) return true;
    else
        return false;
}

void insertAfterNodeSllist(SllistType* li,elementtype element,SllistNode* node)
{
    SllistNode *NewNode;
    NewNode = makeNodeSllist(element);
    if(node==NULL) return;
    else
    {
        NewNode->next = node->next;
        node->next = NewNode;
    }
}


void insertBeforeNodeSllist(SllistType* li,elementtype element,SllistNode* node)
{
    SllistNode *NewNode;
    SllistNode *prev;
    NewNode = makeNodeSllist(element);
    if(node==NULL) return;
    else
    {
        if(node==li->root)
        {
            insertAtBeginningSllist(li,element);
            return;
        }
        prev = prevNode(li,node);
        if(prev!=NULL)
        {
            prev->next = NewNode;
            NewNode->next = node;
        }else
        {
            li->root = NewNode;
            NewNode->next = node;
        }
    }
}

void insertAtBeginningSllist(SllistType* li,elementtype element)
{
    SllistNode* Node;
    Node = makeNodeSllist(element);
    Node->next = li->root;
    li->root = Node;
}

void insertAtEndSllist(SllistType *li,elementtype element)
{
    SllistNode* Node;
    SllistNode* node;
    Node = makeNodeSllist(element);
    if(li->root==NULL)
    {
        li->root = Node;
    }else
    {
        for(node=li->root ; node->next!=NULL ; node=node->next);
        node->next = Node;
    }
}

bool insertAtPositionSlllist(SllistType* li,elementtype element,int n)
{
    int i = 1;
    SllistNode *Node;
    for (Node=li->root; Node != NULL; Node=Node->next)
        {
        if (i == n)
        {
            insertBeforeNodeSllist(li,element,Node);
            return true;
        }
        i++;
        }
    return false;
}

void deleteAtBeginning(SllistType *li)
{
    SllistNode *temp;
    temp = li->root;
    if (li->root == NULL) return;
    li->root = li->root->next;
    free(temp);
}

void deleteAtEnd(SllistType *li)
{
    SllistNode* Node;
    SllistNode* prev;
    if (li->root == NULL) return;
    Node = li->root;
    while(Node->next!=NULL)
        Node = Node->next;
    prev = prevNode(li,Node);
    if(prev!=NULL) prev->next = NULL;
    free(Node);
}

void deleteNode(SllistType *li,SllistNode* node)
{
    SllistNode* prev;
    if(node!=NULL)
    {
        if(node == li->root)
        {
            deleteAtBeginning(li);
            return;
        }
        if(isTailNodeSllist(li,node))
        {
            deleteAtEnd(li);
            return;
        }
        prev = prevNode(li,node);
        if(prev!=NULL)
        {
            prev->next = node->next;
            free(node);
        }

    }
}


void deleteList(SllistType *li)
{
    if (li->root == NULL) return;
    SllistNode *temp;
    temp = li->root;
    while (temp != NULL)
    {
      li->root = li->root->next;
      free(temp);
      temp = li->root;
    }
}

void deleteAtPosition(SllistType* li, int n)
{
  int i = 1;
  SllistNode *Node;
  for (Node=li->root; Node!= NULL; Node=Node->next)
    {
      if (i == n)
      {
        deleteNode(li,Node);
        break;
      }
      i++;
    }
}


SllistNode *searchFunc(SllistType *li,elementtype element)
{
    SllistNode *Node = li->root;
    while (Node != NULL)
    {
      if(func(Node->element,element)==0) return Node;
      Node = Node->next;
    }
    return NULL;
}

void updateSllist(SllistNode* node,elementtype e)
{
    if(node==NULL) return;
    node->element = e;
}

void swapSllist(SllistNode *n1,SllistNode *n2)
{
  elementtype a;
  a = n1->element;
  n1->element = n2->element;
  n2->element = a;
}

void quicksortSllist(SllistType* li)
{
  SllistNode* Node;
  long n, i = 0;
  SllistNode **A;
  n = numberNodeSllist(li);
  A = (SllistNode **)malloc(n * sizeof(SllistNode*));
  for (Node = li->root; Node != NULL; Node = Node->next)
    {
      A[i] = Node;
      i++;
    }
  quicksort2Sllist(A, 0, i-1);
}

void quicksort2Sllist(SllistNode **A, long l, long r)
{
  long i, j;
  SllistNode *p;
  if (l < r)
    {
      p = A[l]; i = l; j = r;
      while (i < j)
	{
	  i++;
	  while (i < r && sosanhSllist(A[i], p) < 0) i++;
	  while (j > l && sosanhSllist(A[j], p) > 0) j--;
	  swapSllist(A[i], A[j]);
	}
      swapSllist(A[i], A[j]); swapSllist(A[l], A[j]);
      quicksort2Sllist(A,l,j-1);
      quicksort2Sllist(A,j+1,r);
    }
}

void printfSllistNode(elementtype element)
{

}

int sosanhSllist(SllistNode* node1,SllistNode* node2)
{
    return 0;
}

int func(elementtype e1, elementtype e2)
{
    return strcmp(e1.username, e2.username);
}

