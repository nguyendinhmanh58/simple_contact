#ifndef SLLIB_H
#define SLLIB_H
#include "./../typedef.h"

///////////////////////////////////////
void printfSllistNode(elementtype element);
int func(elementtype e1, elementtype e2); //for SEARCH
int sosanhSllist(SllistNode* node1,SllistNode* node2); //for SORT
///////////////////////////////////////
SllistNode* makeNodeSllist(elementtype element);
SllistType* makeSllist();
void traverseSllist(SllistType* li);

int numberNodeSllist(SllistType* li);
bool hasPositionSllist(SllistType* li,int i); // Mang bat dau tu 1->n, Neu coi ptu thu 0 la thu 1 thi tim: hasPositionSllist(li,0+1);
SllistNode* findAtPositionSllist(SllistType* li,int i); //Mang bat dau tu 1->n

SllistNode* prevNode(SllistType* li,SllistNode* node);
bool isTailNodeSllist(SllistType* li,SllistNode* node);

void insertAfterNodeSllist(SllistType* li,elementtype element,SllistNode* node);
void insertBeforeNodeSllist(SllistType* li,elementtype element,SllistNode* node);
void insertAtBeginningSllist(SllistType* li,elementtype element);
void insertAtEndSllist(SllistType *li,elementtype element);
void insertAtPositionSllist(SllistType* li,elementtype element,int n); //xet vi tri tu 0, neu xet vi tri tu 1 thi dau vao la n-1

void deleteAtBeginning(SllistType *li);
void deleteAtEnd(SllistType *li);
void deleteCur(SllistType *li);
void deleteList(SllistType *li);
void deleteAtPosition(SllistType* li, int n);

SllistNode *searchFunc(SllistType *li,elementtype element);
void updateSllist(SllistNode* node,elementtype e);
void quicksortSllist(SllistType* li);
void quicksort2Sllist(SllistNode **A, long l, long r);
#endif // SLLIB_H
