#ifndef TYPEDEF_H
#define TYPEDEF_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include "res_code.h"
#include "define.h"

typedef struct Group
{
    char name[30];
    char desc[200];
} Group;

typedef struct Group DllistElementtype;

typedef struct DllistNode DllistNode;

struct DllistNode
{
    DllistElementtype element;
    DllistNode *next;
    DllistNode *prev;
};

typedef struct
{
    DllistNode *head;
    DllistNode *tail;
}DllistType;


typedef struct Contact
{
    char name[50];
    char phone[12];
    char email[30];
    char address[100];
    //char group_name[30];
    DllistType* group_names;
    int favorite;
    char note[500];
} Contact;

typedef struct Contact bTreeElementtype;

typedef struct SecretQuestion
{
    int question_id;
    char question[100];
    char answer[100];
} SecretQuestion;

typedef struct elementtype
{
    char username[30];
    char password[50];
    char firstname[20];
    char lastname[20];
    int time_lock;
    SecretQuestion secret_questions[NUMBER_SECRET_QUESTION];
} elementtype;

typedef struct elementtype Account;

typedef struct Res
{
    enum Code code;
    char value[1024];
} Res;

typedef struct SllistNode SllistNode;

struct SllistNode{
    elementtype element;
    SllistNode *next;
};

typedef struct
{
    SllistNode *root;
} SllistType;

typedef struct TreeNode TreeNode;

struct TreeNode
{
    bTreeElementtype element;
    TreeNode* left;
    TreeNode* right;
};

typedef TreeNode* TreeType;


typedef struct ClientInfo
{
    int status;
    int password_fail_times;
    Account current_account;
    TreeType contacts;
    DllistType *groups;
} ClientInfo;
#endif // TYPEDEF_H
