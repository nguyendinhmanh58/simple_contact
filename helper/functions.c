#include "functions.h"
#include "./../libs/typedef.h"
#include <string.h>
char* getStrByIndex(char *str, int index, char* split_char)
{
    char* str_cpy;
    char* result;
    int i = 0;
    str_cpy = (char*)malloc(sizeof(char)*(strlen(str)+1));
    strcpy(str_cpy, str);
    result = strtok(str_cpy, split_char);
    if(index == 0) return result;
    while((result = strtok(NULL, split_char))!= NULL){
        i++;
        if(i == index) return result;
    }
    return NULL;
}

