#ifndef CLIENT_LIB_H
#define CLIENT_LIB_H
#include "./../../libs/md5/md5.h"
#include "./../../libs/typedef.h"
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <stdbool.h>

#define SIZE 50
#define MAX_LENGTH_ID 30
#define MIN_LENGTH_ID 6
int client_sock;
bool isValidIpAddress(char *ipAddress);
void menuStart();
void processResponse(Res response);
void sendRequest(Res request);
void receiveResponse(Res* response);
void messageError();
void menuInputPass();
void showMessage(Res response);
void getPassword(char str[]);
#endif // CLIENT_LIB_H
