#include "client_lib.h"
void menuStart()
{
    char choose[10];
    int choose_int;
    do
    {
        //system("clear");
        printf(ANSI_COLOR_YELLOW "\t\t\t#####  SIMPLE CONTACT #####\n");
        printf("\t\t\t#                         #\n");
        printf("\t\t\t#    1. LOGIN             #\n");
        printf("\t\t\t#    2. SIGN UP           #\n");
        printf("\t\t\t#    3. QUIT              #\n");
        printf("\t\t\t#                         #\n");
        printf("\t\t\t###########################\n" ANSI_COLOR_RESET);
        printf("\t\t\tChoose(1/2/3): ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
        while(choose_int!=1 && choose_int!=2 && choose_int!=3 ){
            printf("\t\t\tYour choose do not exist. Try again\n");
            printf("\t\t\tChoose(1 or 2 or 3): ");
            scanf("%[^\n]%*c",choose);
            choose_int = atoi(choose);
        }
        switch(choose_int)
        {
        case 1:
            loginUserId();
            break;
        case 2:
            signup();
            break;
        case 3:
            exit(0);
        default:
            break;
        }
    }while(choose_int!=3);
}
void processResponse(Res response)
{
    switch(response.code)
    {
    case LOGIN_USER_OK:
        menuInputPass();
        break;
    case LOGIN_USER_ERROR:
        showMessage(response);
        menuStart();
        break;
    case LOGIN_PASS_OK:
        showMessage(response);
        menuContactManager();
        break;
    case LOGIN_PASS_ERROR:
        showMessage(response);
        menuInputPass();
        break;
    case BLOCK:
        showMessage(response);
        menuStart();
        break;
    case SIGN:
        signUserId();
        break;
    case ERROR_SIGN_USER_EXIST:
        showMessage(response);
        menuStart();
        break;
    case SIGN_USER_OK:
        signOtherInfo(response);
        break;
    case SIGN_OK:
        showMessage(response);
        //printf("\t\t\tSignup successful\n\n");
        menuStart();
        break;
    case FORGOT_PASS_OK:
        forgotPassword(response);
        break;
    case CORRECT_ANSWER:
        showMessage(response);
        break;
    case ERROR_ANSWER:
        showMessage(response);
        menuStart();
        break;
    case INPUT_NEW_PASS:
        inputNewPassword();
        break;
    case CHANGE_PASS_OK:
        showMessage(response);
        menuStart();
        break;
    case INPUT_CONTACT_INFO:
        // showMessage(response);
        inputAddContact(response);
        break;
    case ADD_CONTACT_OK:
        showMessage(response);
        menuContactManager();
        break;
    case ADD_GROUP_OK:
        showMessage(response);
        menuContactManager();
        break;
    case ERROR_GROUP_NAME_EXISTED:
        showMessage(response);
        menuContactManager();
        break;
    case ERROR_EXISTS_IN_FAVORITE:
        showMessage(response);

        //menuContactManager();
        break;
    case ERROR_CONTACT_NOT_EXISTS:
        showMessage(response);
        menuContactManager();
        break;
    case ERROR_CONTACT_NAME_EXISTED:
        showMessage(response);
        menuContactManager();
        break;
    case ADD_FAVORITE_OK:
        showMessage(response);
        //menuContactManager();
        break;
    case FOUNDED_CONTACTS:
        showListContactToView(response);
        menuActionContactViewed(response);
        break;
    case NOT_FOUND_CONTACT:
        showMessage(response);
        menu_search_contact();
        break;
    case FOUNDED_CONTACT:
        showContactViewed(response);
        menuActionContactViewed(response);
        break;
    case CHOOSE_GROUP:
        //showMessage(response);
        break;
    case ADD_CONTACT_TO_GROUPS_RESULT:
        showMessage(response);
        //menuActionContactViewed(response);
        break;
    case ERROR_GROUP_NOT_EXISTS:
        showMessage(response);
        menuActionContactViewed(response);
        break;
    case ERROR_EXISTS_IN_GROUP:
        showMessage(response);
        //menuActionContactViewed(response);
        break;
    case UPDATE_CONTACT_OK:
        //showMessage(response);
        //menuActionContactViewed(response);
        showContactViewed(response);
        update_contact(response);
        break;
    case DELETE_CONTACT_OK:
        showMessage(response);
        menu_search_contact();
        break;
    case FOUNDED_GROUP:
        showGroupInfo(response);
        break;
    case NOT_FOUND_GROUP:
        showMessage(response);
        menuSearchGroup();
        break;
    case UPDATE_GROUP_OK:
        showGroupInfo(response);
        break;
    case DELETE_GROUP_OK:
        showMessage(response);
        menuSearchGroup();
        break;
    case REMOVE_CONTACTS_FROM_GROUP_RESULT:
        //showGroupInfo(response);
        showMessage(response);
        //menuSearchGroup();
        break;
    case FAVORITE:
        showFavorite(response);
        menuFavoriteManager();
        break;
    case DELETE_CONTACTS_IN_FAVORITE_RESULT:
        showMessage(response);
        menuFavoriteManager();
        break;
    case ERROR_CONTACT_NOT_EXITS_IN_FAVORITE:
        showMessage(response);
        menuContactManager();
        break;
    case BACKUP_ERROR_CANNOT_SET_NAME_COINCIDE_WITH_FILE_DATA:
        showMessage(response);
        menuContactManager();
        break;
    case BACKUP_ERROR_FILE_ALREADY_EXISTED:
        showMessage(response);
        //replaceFileBackup();

        break;
    case BACKUP_ERROR_CANNOT_MAKE_FILE:
        showMessage(response);
        menuContactManager();
        break;
    case BACKUP_OK:
        showMessage(response);
        menuContactManager();
        break;
    case ERROR_NO_FILE:
        showMessage(response);
        menuContactManager();
        break;
    case ALL_FILES:
        break;
    case RESTORE_OK:
        showMessage(response);
        menuContactManager();
        break;
    case FILE_NOT_EXIST:
        showMessage(response);
        menuContactManager();
        break;
    case LOGOUT_OK:
        showMessage(response);
        menuStart();
        break;
    case RE_LOGIN_OK:
        menuStart();
        break;
    case ERROR_RESET_PASSWORD:
        showMessage(response);
        menuContactManager();
        break;
    default:
        showMessage(response);
        menuStart();
        break;
    }
}
void sendRequest(Res request){
    int bytes_sent = 0;
    int relay_count = 0;
    bytes_sent = send(client_sock,&request,sizeof(request),0);
    //printf("client_sock==>>%d<\n",client_sock);
    while(bytes_sent<0)
    {
        messageError();
        bytes_sent = send(client_sock,&request,sizeof(request),0);
    }
    //printf("send ok %d\n", bytes_sent);
}

void receiveResponse(Res* response){
    int bytes_received;
    bytes_received = recv(client_sock,response,sizeof(Res),0);
    while(bytes_received<0)
    {
        messageError();
        bytes_received = recv(client_sock,response,sizeof(Res),0);
    }
}

void showMessage(Res response){
    printf("\n\t\t\t%s\n", response.value);
}
void messageError()
{
    printf("\t\t\t#################\n");
    printf("\t\t\t# Connect error #\n");
    printf("\t\t\t#################\n");
    //client_connect();
}
void menuInputPass()
{
    char choose[10];
    int chon;
    char s1[30];
    char old_pass[20];
    Res request, response;
    printf("\t\t\t############################\n");
    printf("\t\t\t# 1. INPUT PASSWORD        #\n");
    printf("\t\t\t# 2. FOGOT PASSWORD        #\n");
    printf("\t\t\t# 3. BACK                  #\n");
    printf("\t\t\t############################\n");
    printf("\t\t\tChoose: ");
    scanf("%[^\n]%*c",choose);
    chon = atoi(choose);
    while(chon != 1 && chon != 2 && chon != 3){
        printf("\t\t\tYour choose do not exist. Try again\n");
        printf("\t\t\tChoose: ");
        scanf("%[^\n]%*c",choose);
	chon = atoi(choose);
    }
    switch(chon)
    {
    case 1:
        printf("\t\t\tInput password: ");
        getPassword(s1);
        strcpy(request.value, str2md5(s1));
        request.code = LOGIN_PASS;
        sendRequest(request);
        receiveResponse(&response);
        processResponse(response);
        break;
    case 2:
        request.code = FORGOT_PASS;
        sendRequest(request);
        receiveResponse(&response);
        processResponse(response);
        break;
    case 3:
        request.code = RE_LOGIN;    //re login
        sendRequest(request);
        receiveResponse(&response);
        processResponse(response);
        menuStart();
        break;
    }
}

void getPassword(char password[])
{
    static struct termios oldt, newt;
    int i = 0;
    int c;

    /*saving the old settings of STDIN_FILENO and copy settings for resetting*/
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;

    /*setting the approriate bit in the termios struct*/
    newt.c_lflag &= ~(ECHO);

    /*setting the new bits*/
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    /*reading the password from the console*/
    while ((c = getchar())!= '\n' && c != EOF && i < SIZE)
    {
        password[i++] = c;
    }
    password[i] = '\0';

    /*resetting our old STDIN_FILENO*/
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

}
bool isValidIpAddress(char *ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return result != 0;
}
