#include "forgot_pass.h"
void forgotPassword(Res response) {
    Res request;
    char s[256];
    char tmp[50];
    char answer[30];
    char question[30];
    int questionID[4];
    int total_question;
    strcpy(s,response.value);
    strcpy(tmp,getStrByIndex(s,0,"|"));
    total_question = atoi(tmp);
    request.code = ANSWER_QUESTION;
    for(int i=1;i<=total_question;i++)
    {
        strcpy(tmp,getStrByIndex(s,i,"|"));
        strcpy(questionID,getStrByIndex(tmp,0,"-"));
        strcpy(question,getStrByIndex(tmp,1,"-"));
        //sscanf(tmp,"%s-%s",questionID,question);
        printf("\n\t\t\t%s\n",question);
        printf("\t\t\tInput your answer: ");
        scanf("%[^\n]%*c",answer);
        strcpy(request.value,questionID);
        strcat(request.value,"-");
        strcat(request.value,answer);
        //printf("-------%s\n",request.value);
        //sprintf(request.value,"%s-%s",questionID,answer);
        sendRequest(request);
        receiveResponse(&response);
        processResponse(response);
    }
}
void inputNewPassword() {
    char newPass[30], password_confirm[30];
    Res request, response;
    printf("\n\t\t\tInput new password: ");
    //scanf("%[^\n]%*c",password);
    getPassword(newPass);
    while(strlen(newPass)<8 || strlen(newPass)>30)
    {
        printf("\n\t\t\tPassword invalid\n");
        printf("\n\t\t\tInput new password: ");
        //scanf("%[^\n]%*c",password);
        getPassword(newPass);
    }
    printf("\n\t\t\tInput confirm password: ");
    //scanf("%[^\n]%*c",password_confirm);
    getPassword(password_confirm);
    while(strcmp(newPass,password_confirm)!=0)
    {
        printf("\n\t\t\tConfirm password error\n");
        inputNewPassword();
    }
    request.code = REGISTER_NEW_PASS;
    strcpy(request.value,str2md5(newPass));
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
