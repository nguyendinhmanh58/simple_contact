#include "./libs/client_lib.h"
#include "./login/login.h"
#include "./signup/signup.h"
#include "./forgot_pass/forgot_pass.h"



int main()
{
    client_connect();
    return 0;
}
void client_connect()
{
    int recv_byte;
    char buff[1024];
    char recv_data[1024];
    struct sockaddr_in server_addr;
    int bytes_sent,bytes_received;
    bool checIpAddress;
    char IpAddress[20];
    client_sock=socket(AF_INET,SOCK_STREAM,0);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    do
    {
        printf("\t\t\tInput server address: ");
        scanf("%[^\n]%*c",IpAddress);
        checIpAddress = isValidIpAddress(IpAddress);
        if(checIpAddress==true)
        break;
        printf("\t\t\tAddress is invalid.\n");
    }while(1);


    server_addr.sin_addr.s_addr = inet_addr(IpAddress);
    if(connect(client_sock,(struct sockaddr*)&server_addr,sizeof(struct sockaddr))!=0)
    {
        printf("\n\t\t\tError!Can not connect to sever!Client exit imediately! \n");
        //sleep(3);
        return 0;
    }

    bytes_received = recv(client_sock,buff,1024,0);
    if(bytes_received == -1)
    {
        printf("\nError!Cannot receive data from sever!\n");
        close(client_sock);
        exit(-1);
    }
    buff[bytes_received] = '\0';
    menuStart();
}

