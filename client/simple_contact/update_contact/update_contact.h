#ifndef UPDATE_CONTACT_H
#define UPDATE_CONTACT_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"
#include "./../../../helper/functions.h"
#include "./../add_to_group/add_to_group.h"

void update_contact(Res response);

#endif // UPDATE_CONTACT_H
