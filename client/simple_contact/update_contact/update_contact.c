#include "update_contact.h"

void update_contact(Res response)
{
    Res new_response, request;
    char choose[10], contact_name[50], phone[12], email[30], address[100], group_names[100], note[500], favorite_char[5];
    int choose_int, choose_group;
    int favorite = 0, total_groups = 0, count = 0;
  printf("\n%s\t\t\t######## UPDATE CONTACT #########\n",ANSI_COLOR_BLUE);
    printf("\t\t\t# 1. Update contact name        #\n");
    printf("\t\t\t# 2. Update phone of contact    #\n");
    printf("\t\t\t# 3. Update email of contact    #\n");
    printf("\t\t\t# 4. Update address of contact  #\n");
    printf("\t\t\t# 5. Update group of contact    #\n");
    printf("\t\t\t# 6. Update favorite of contact #\n");
    printf("\t\t\t# 7. Update note of contact     #\n");
    printf("\t\t\t# 8. Update all information     #\n");
    printf("\t\t\t# 9. BACK                       #\n");
    printf("\t\t\t#################################%s\n", ANSI_COLOR_RESET);
    printf("\t\t\tChoose: ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 &&choose_int!=3 &&choose_int!=4 &&choose_int!=5 &&choose_int!=6 &&choose_int!=7 &&choose_int!=8 &&choose_int!=9)
    {
        printf("\t\t\tChoose error(1->9). Input again: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    request.code = UPDATE_CONTACT;
    strcpy(contact_name,getStrByIndex(response.value,0,"|"));
    strcpy(phone,getStrByIndex(response.value,1,"|"));
    strcpy(email,getStrByIndex(response.value,2,"|"));
    strcpy(address,getStrByIndex(response.value,3,"|"));
    strcpy(group_names,getStrByIndex(response.value,4,"|"));
    favorite = atoi(getStrByIndex(response.value,5,"|"));
    strcpy(note,getStrByIndex(response.value,6,"|"));
    switch(choose_int)
    {
        case 1:
            printf("\t\t\tInput new contact name: ");
            scanf("%[^\n]%*c",contact_name);
            while(strlen(contact_name)<1||strlen(contact_name)>50)
            {
                printf("\t\t\tContact name invalid(can't blank, length maximum 50). Input again: ");
                scanf("%[^\n]%*c",contact_name);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            //printf("====>%s\n",request.value);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 2:
            printf("\t\t\tInput new phone of contact : ");
            scanf("%[^\n]%*c",phone);
            while(strlen(phone)>13)
            {
                printf("\t\t\tPhone of contact is invalid(length maximum 13). Input again: ");
                scanf("%[^\n]%*c",phone);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);

            break;
        case 3:
            printf("\t\t\tInput new email of contact : ");
            scanf("%[^\n]%*c",email);
            while(strlen(email)<6||strlen(email)>30||strstr(email,"@")==0||strstr(email,".")==0)
            {
                printf("\t\t\tEmail of contact is invalid. Input again: ");
                scanf("%[^\n]%*c",email);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 4:
            printf("\t\t\tInput new address of contact : ");
            scanf("%[^\n]%*c",address);
            while(strlen(address)<6||strlen(address)>100)
            {
                printf("\t\t\tAddress of contact is valid. Input again: ");
                scanf("%[^\n]%*c",address);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 5:
            getAllGroup(&new_response);
            count=0;
            total_groups = atoi(getStrByIndex(new_response.value,0,"|"));

            if(total_groups==0)
            {
                printf("\t\t\tList group empty.\n");
                update_contact(response);
            }
            else{
                printf("\n\t\t\tList group\n");
                for(int i= 1; i<=total_groups; i++)
                {
                    printf("\t\t\t%d - %s\n",i,getStrByIndex(new_response.value, i,"|"));
                }
                printf("\t\t\t%d - %s\n",(total_groups+1),"No group");
                printf("\t\t\tChoose group(1,2,..): ");
                scanf("%[^\n]%*c",choose);
                for(int i = 0; i<strlen(choose); i++)
                {
                    if(choose[i]==',')
                        count++;
                }
                if(count==0)
                {
                    choose_int = atoi(choose);
                    while(choose_int <= 0 || choose_int > (total_groups+1))
                    {
                        printf("\t\t\tChoose error. input again: ");
                        scanf("%[^\n]%*c",choose);
                        choose_int = atoi(choose);
                    }
                    if(choose_int==(total_groups+1))
                        strcpy(group_names,"0");
                    else
                        sprintf(group_names,"%d-%s",1,getStrByIndex(new_response.value,choose_int,"|"));
                }
                else {
                    sprintf(group_names,"%d",(count+1));
                    for(int i=0 ; i <= count; i++)
                    {
                        strcat(group_names,"-");
                        choose_group = atoi(getStrByIndex(choose,i,","));
                        strcat(group_names,getStrByIndex(new_response.value,choose_group,"|"));
                    }
                }
                sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
                sendRequest(request);
                receiveResponse(&new_response);
                processResponse(new_response);
            }


            break;
        case 6:
            printf("\t\t\tFavorite(yes/no): ");
            scanf("%[^\n]%*c",favorite_char);
            while(strcmp(favorite_char,"yes")!=0 && strcmp(favorite_char,"no")!=0 )
            {
                printf("\t\t\tChoose error. Input again: ");
                scanf("%[^\n]%*c",favorite_char);
            }
            if(strcmp(favorite_char,"yes")==0)
                favorite = 1;
            else
                favorite = 0;

            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 7:
            printf("\t\t\tInput new note: ");
            scanf("%[^\n]%*c",note);
            while(strlen(note)<1|| strlen(note)>500)
            {
                printf("\t\t\tNote error. Input again: ");
                scanf("%[^\n]%*c",note);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;

        case 8:
            printf("\t\t\tInput new contact name: ");
            scanf("%[^\n]%*c",contact_name);
            while(strlen(contact_name)<3||strlen(contact_name)>50)
            {
                printf("\t\t\tContact name invalid. Input again: ");
                scanf("%[^\n]%*c",contact_name);
            }
            printf("\t\t\tInput new phone of contact : ");
            scanf("%[^\n]%*c",phone);
            while(strlen(phone)<10||strlen(phone)>13)
            {
                printf("\t\t\tPhone of contact is invalid. Input again: ");
                scanf("%[^\n]%*c",phone);
            }
            printf("\t\t\tInput new email of contact : ");
            scanf("%[^\n]%*c",email);
            while(strlen(email)<6||strlen(email)>30||strstr(email,"@")==0||strstr(email,".")==0)
            {
                printf("\t\t\tEmail of contact is valid. Input again: ");
                scanf("%[^\n]%*c",email);
            }
            printf("\t\t\tInput new address of contact : ");
            scanf("%[^\n]%*c",address);
            while(strlen(address)<6||strlen(address)>100)
            {
                printf("\t\t\tAddress of contact is invalid. Input again: ");
                scanf("%[^\n]%*c",address);
            }
            getAllGroup(&new_response);
            total_groups = atoi(getStrByIndex(new_response.value,0,"|"));
            if(total_groups==0)
            {
                printf("\t\t\tList group empty. Can't add this contact to group\n");
            } else {
                printf("\t\t\tList group\n");
                for(int i= 1; i<=total_groups; i++)
                {
                    printf("\t\t\t%d - %s\n",i,getStrByIndex(new_response.value, i,"|"));
                }
                printf("\t\t\t%d - %s\n",(total_groups+1),"No group");
                printf("\t\t\tChoose group(1,2,..): ");
                scanf("%[^\n]%*c",choose);
                for(int i = 0; i<strlen(choose); i++)
                {
                    if(choose[i]==',')
                        count++;
                }
                if(count==0)
                {
                    choose_int = atoi(choose);
                    while(choose_int <= 0 || choose_int > (total_groups+1))
                    {
                        printf("Choose error. input again: ");
                        scanf("%[^\n]%*c",choose);
                        choose_int = atoi(choose);
                    }
                    if(choose_int==(total_groups+1))
                        strcpy(group_names,"0");
                    else
                        sprintf(group_names,"%d-%s",1,getStrByIndex(new_response.value,choose_int,"|"));
                }
                else {
                    sprintf(group_names,"%d",(count+1));
                    for(int i=0 ; i <= count; i++)
                    {
                        strcat(group_names,"-");
                        choose_group = atoi(getStrByIndex(choose,i,","));
                        if(choose_group==(total_groups+1))
                        {
                            printf("\t\t\tChoose error.\n");
                            update_contact(response);
                            break;
                        }
                        else
                            strcat(group_names,getStrByIndex(new_response.value,choose_group,"|"));
                    }
                }
            }


            printf("\t\t\tFavorite(yes/no): ");
            scanf("%[^\n]%*c",favorite_char);
            while(strcmp(favorite_char,"yes")!=0 && strcmp(favorite_char,"no")!=0 )
            {
                printf("\t\t\tChoose error(yes/no). Input again: ");
                scanf("%[^\n]%*c",favorite_char);
            }
            if(strcmp(favorite_char,"yes")==0)
                favorite = 1;
            else
                favorite = 0;
            printf("\t\t\tInput new note: ");
            scanf("%[^\n]%*c",note);
            while(strlen(note)<1|| strlen(note)>500)
            {
                printf("\t\t\tNote error(note can't blank and maximum 500 character). Input again: ");
                scanf("%[^\n]%*c",note);
            }
            sprintf(request.value,"%s|%s|%s|%s|%s|%s|%d|%s",getStrByIndex(response.value,0,"|"),contact_name,phone,email,address,group_names,favorite,note);
            sendRequest(request);
            //printf("====> %s\n",request.value);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 9:
            menuActionContactViewed(response);
            break;
    }
}
