#include "add_favorite.h"
void addFavorite()
{
    Res request, response;
    request.code = ADD_FAVORITE;
    printf("Input contact name: ");
    scanf("%[^\n]%*c",request.value);
    while(strlen(request.value)==0)
    {
        printf("Contact name can't blank. Input again: ");
        scanf("%[^\n]%*c",request.value);
    }
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
