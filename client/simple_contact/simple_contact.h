#ifndef SIMPLE_CONTACT_H
#define SIMPLE_CONTACT_H
#include "./../libs/client_lib.h"
#include "./../../libs/typedef.h"
#include "./add_contact/add_contact.h"
#include "./add_group/add_group.h"
#include "./add_favorite/add_favorite.h"
#include "./search_contact/search_contact.h"

void menuContactManager();
#endif // SIMPLE_CONTACT_H
