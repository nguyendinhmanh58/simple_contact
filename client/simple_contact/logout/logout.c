#include "logout.h"

void logout()
{
    Res request, response;
    request.code  = LOGOUT;
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
