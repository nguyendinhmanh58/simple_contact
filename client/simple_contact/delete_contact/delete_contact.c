#include "delete_contact.h"
void delete_contact(Res response)
{
    Res request, new_response;
    request.code = DELETE_CONTACT;
    strcpy(request.value,getStrByIndex(response.value,0,"|"));
    sendRequest(request);
    receiveResponse(&new_response);
    processResponse(new_response);
}
