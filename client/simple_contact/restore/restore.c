#include "restore.h"

void restore()
{
    int number_files, choose_int;
    char choose[10];
    char confirm_restore[10];
    Res request, response, new_response;
    //getAllFile(&response);
    request.code = GET_ALL_FILES;
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
    number_files = atoi(getStrByIndex(response.value,0,"|"));
    request.code = RESTORE;
    if(number_files==1)
    {
        printf("\t\t\tYou have only one file to restore. You want to restore with file: %s\n",getStrByIndex(response.value,1,"|"));
        printf("\t\t\tYes/No: ");
        scanf("%[^\n]%*c",choose);
        while(strcmp(choose,"yes")!= 0 && strcmp(choose,"no")!= 0)
        {
            printf("\t\t\tChoose yes or no: ");
            scanf("%[^\n]%*c",choose);
        }
        if(strcmp(choose,"yes") == 0)
        {
            strcpy(request.value,getStrByIndex(response.value,1,"|"));
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
        } else {
            menuContactManager();
        }
    } else {
        printf("\n\t\t\tLIST FILE:\n");
        for(int i = 1; i <= number_files; i++)
        {
            printf("\t\t\t%d - %s\n",i,getStrByIndex(response.value,i,"|"));
        }
        printf("\t\t\t%d - BACK\n",(number_files+1));
        printf("\t\t\tChoose file to send or back: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
        while(choose_int < 1 || choose_int > (number_files+1))
        {
            printf("\t\t\tChoose error. Choose(1 -> %d): ",(number_files+1));
            scanf("%[^\n]%*c",choose);
            choose_int = atoi(choose);
        }
        if(choose_int==number_files+1)
        menuContactManager();
        else
        {
            printf("\t\t\tYou want to restore from file(yes/no): %s", getStrByIndex(response.value,choose_int,"|"));
            scanf("%[^\n]%*c",confirm_restore);
            while(strcmp(confirm_restore,"yes")!=0 && strcmp(confirm_restore,"no")!=0)
            {
                printf("\t\t\tChoose error(yes/no): ");
                scanf("%[^\n]%*c",confirm_restore);
            }
            if(strcmp(confirm_restore,"yes")==0)
            {
                if(choose_int==(number_files+1)) menuContactManager();
                else {
                    strcpy(request.value,getStrByIndex(response.value,choose_int,"|"));
                    sendRequest(request);
                    receiveResponse(&new_response);
                    processResponse(new_response);
                }
            } else {
                menuContactManager();
            }

        }

    }
}
void getAllFile(Res* response)
{
    Res request;
    request.code = GET_ALL_FILES;
    sendRequest(request);
    receiveResponse(response);
    processResponse(*response);
}
