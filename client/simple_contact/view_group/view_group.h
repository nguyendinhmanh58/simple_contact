#ifndef VIEW_CONTACT_H
#define VIEW_CONTACT_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"
#include "./../add_to_group/add_to_group.h"

void menuSearchGroup();
void showGroupInfo(Res response);
void updateGroup(Res response);
void deleteGroup(Res response);

#endif // VIEW_CONTACT_H
