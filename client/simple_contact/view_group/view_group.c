#include "view_group.h"
void menuSearchGroup()
{
    Res response, request, new_response;
    char choose[10], value[50];
    int choose_int, number_groups;
    getAllGroup(&response);
    number_groups = atoi(getStrByIndex(response.value,0,"|"));
    if(number_groups==0)
    {
        printf("\t\t\tList group empty\n");
        menuContactManager();
    }
    else
    {
        request.code = VIEW_GROUP;
        if(number_groups==1)
        {
            strcpy(request.value,getStrByIndex(response.value,1,"|"));
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
        } else {
            printf(ANSI_COLOR_YELLOW, "\t\t\tLIST GROUP:\n");
            for(int i = 1; i<=number_groups;i++)
            {
                printf("\t\t\t%d - %s\n",i, getStrByIndex(response.value,i,"|"));
            }
            printf("\t\t\t%d - BACK\n" ANSI_COLOR_RESET,number_groups+1);
            printf("\t\t\tChoose(1 -> %d): ",number_groups+1);
            scanf("%[^\n]%*c",choose);
            choose_int = atoi(choose);
            while(choose_int!=1 && choose_int !=2 && choose_int!=3)
            {
                printf("\t\t\tChoose error. Choose again(1 -> %d): ",number_groups+1);
                scanf("%[^\n]%*c",choose);
                choose_int = atoi(choose);
            }
            if(choose_int==(number_groups+1))
            {
                menuContactManager();
            } else {
                strcpy(request.value,getStrByIndex(response.value,choose_int,"|"));
                sendRequest(request);
                receiveResponse(&new_response);
                processResponse(new_response);
            }
        }
    }


}
void showGroupInfo(Res response)
{
    int choose_int;
    char choose[10];
    int number_contacts, number_groups;
    Res new_response;
    number_contacts = atoi(getStrByIndex(response.value,2,"|"));
    //printf("\t\t\t############################################\n");
    printf("\t\t\t __________________________________________\n");
    printf("\t\t\t|Group name: %30s|\n",getStrByIndex(response.value,0,"|"));
    printf("\t\t\t|Group desc: %30s|\n",getStrByIndex(response.value,1,"|"));
    printf("\t\t\t|List contact: %d|\n",number_contacts);
    for(int i = 3;i <= (number_contacts+2);i++)
    {
        printf("\t\t\t|Contact %d   %30s|\n",i-2,getStrByIndex(response.value,i,"|"));
    }
    //printf("\t\t\t############################################\n");
    printf("\t\t\t|__________________________________________|\n");
    printf("\n%s\t\t\t###### GROUP MANAGER ######\n",ANSI_COLOR_CYAN);
    printf("\t\t\t# 1. Update this group    #\n");
    printf("\t\t\t# 2. Delete this group    #\n");
    printf("\t\t\t# 3. BACK                 #\n");
    printf("\t\t\t###########################%s\n",ANSI_COLOR_RESET );
    printf("\t\t\tChoose: ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3)
    {
        printf("\t\t\tChoose error. Choose again: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    switch(choose_int)
    {
        case 1:
            updateGroup(response);
            break;
        case 2:
            deleteGroup(response);
            break;
        case 3:
            //menuSearchGroup();
            getAllGroup(&new_response);
            number_groups = atoi(getStrByIndex(new_response.value,0,"|"));
            if(number_groups==1)
                menuContactManager();
            else
                menuSearchGroup();
            break;
        default:
            menuContactManager();
            break;
    }
}
void updateGroup(Res response)
{
    Res request, new_response;
    char choose[10], new_group_name[50], new_desc[500], list_contacts[200];
    int choose_int, number_contacts, count, flag, tmp;
    number_contacts = atoi(getStrByIndex(response.value,2,"|"));
    request.code = UPDATE_GROUP;
    printf("\t\t\t##### UPDATE GROUP INFO ######\n");
    printf("\t\t\t# 1. Change group name       #\n");
    printf("\t\t\t# 2. Change desc of group    #\n");
    printf("\t\t\t# 3. Delete contact in group #\n");
    printf("\t\t\t# 4. BACK                    #\n");
    printf("\t\t\t##############################\n");
    printf("\t\t\tChoose action(1/2/3/4/): ");
    scanf("%[^\n]%*c",choose);
    choose_int= atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3 && choose_int!=4)
    {
        printf("Choose error. Choose again(1/2/3/4): ");
        scanf("%[^\n]%*c",choose);
        choose_int= atoi(choose);
    }
    switch(choose_int)
    {
        case 1:
            printf("\t\t\tInput new name of group:");
            scanf("%[^\n]%*c", new_group_name);

            while(strlen(new_group_name)==0)
            {
                printf("\t\t\tGroup name can't blank. Input again: ");
                scanf("%[^\n]%*c", new_group_name);
            }
            strcpy(request.value,getStrByIndex(response.value,0,"|"));
            strcat(request.value,"|");
            strcat(request.value,new_group_name);
            strcat(request.value,"|");
            strcat(request.value,getStrByIndex(response.value,1,"|"));
            sprintf(list_contacts,"|%d",number_contacts);
            for(int i = 3; i <=(number_contacts+2);i++)
            {
                strcat(list_contacts,"|");
                strcat(list_contacts,getStrByIndex(response.value,i,"|"));
            }
            strcat(request.value,list_contacts);
            //printf("request value=====>%s\n",request.value);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 2:
            printf("t\t\tInput new desc fof group: ");
            scanf("%[^\n]%*c", new_desc);

            while(strlen(new_desc)==0)
            {
                printf("t\t\tGroup name can't blank. Input again: ");
                scanf("%[^\n]%*c", new_desc);
            }
            strcpy(request.value,getStrByIndex(response.value,0,"|"));
            strcat(request.value,"|");
            strcat(request.value,getStrByIndex(response.value,0,"|"));
            strcat(request.value,"|");
            strcat(request.value,new_desc);
            sprintf(list_contacts,"|%d",number_contacts);
            for(int i = 3; i <=(number_contacts+2);i++)
            {
                strcat(list_contacts,"|");
                strcat(list_contacts,getStrByIndex(response.value,i,"|"));
            }
            strcat(request.value,list_contacts);
            //printf("request value=====>%s\n",request.value);
            sendRequest(request);
            receiveResponse(&new_response);
            processResponse(new_response);
            break;
        case 3:
            request.code = REMOVE_CONTACTS_FROM_GROUP;
            if(number_contacts==0)
            {
                printf("\t\t\tList contact empty\n");
                updateGroup(response);
            } else {
                printf("\n\t\t\t#List contact: %d\n",number_contacts);

                for(int i = 3;i <= (number_contacts+2);i++)
                {
                    printf("\t\t\t#Contact %d   %30s#\n",i-2,getStrByIndex(response.value,i,"|"));
                }
                count = 0;
                printf("\n\t\t\tChoose contacts to delete in group(validate: 1,2..): ");
                scanf("%[^\n]%*c",choose);
                for(int i = 0; i<strlen(choose); i++)
                {
                    if(choose[i]==',')
                    count++;
                }
                sprintf(list_contacts,"|%d",(count+1));
                if(count==0)
                {
                    choose_int = atoi(choose);
                    if(choose_int > number_contacts)
                    {
                        printf("\t\t\tChoose error.\n");
                        updateGroup(response);
                    }
                    else
                    {
                        sprintf(list_contacts,"|%d|",choose_int);
                        strcat(list_contacts,getStrByIndex(response.value,(choose_int+2),"|"));
                    }
                } else {
                    for(int i = 0; i <= count; i++)
                    {
                        strcat(list_contacts,"|");
                        tmp = atof(getStrByIndex(choose,i,",")) + 2;
                        strcat(list_contacts,getStrByIndex(response.value,tmp,"|"));
                    }
                }
                strcat(request.value,getStrByIndex(response.value,0,"|"));
                strcat(request.value,list_contacts);
                //printf("request value=====>%s\n",request.value);
                sendRequest(request);
                receiveResponse(&new_response);
                //printf("response====>#%d-%s#\n",new_response.code,new_response.value);
                processResponse(new_response);
                request.code = VIEW_GROUP;
                strcpy(request.value,getStrByIndex(response.value,0,"|"));
                sendRequest(request);
                receiveResponse(&response);
                showGroupInfo(response);
                updateGroup(response);
            }

            break;
        case 4:
            menuSearchGroup();
            break;
        default:
            menuSearchGroup();
            break;


    }
}
void deleteGroup(Res response)
{
    Res request, new_response;
    char choose[10];
    printf("\t\t\tYou want to delete this group(yes/no)?");
    scanf("%[^\n]%*c",choose);
    while(strcmp(choose,"yes")!=0 && strcmp(choose,"no")!=0 )
    {
        printf("\t\t\tChoose again(yes/no): ");
        scanf("%[^\n]%*c",choose);
    }
    if(strcmp(choose,"yes")==0)
    {
        request.code = DELETE_GROUP;
        strcpy(request.value,getStrByIndex(response.value,0,"|"));
        sendRequest(request);
        receiveResponse(&new_response);
        processResponse(new_response);
    }
    else
    {
        showGroupInfo(response);
    }
}
