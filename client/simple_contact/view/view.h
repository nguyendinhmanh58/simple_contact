#ifndef VIEW_H
#define VIEW_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"

void menuView();
void menuViewContact();
void showContactViewed(Res response);
void menuViewFavorite();

#endif // VIEW_H
