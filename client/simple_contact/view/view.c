#include "view.h"
void menuView() {
    char choose_char;
    int choose;
    printf("\n<<======View======>>\n");
    printf("1. View contact\n");
    printf("2. View favorite\n");
    printf("3. View Group\n");
    printf("4. BACK\n");
    printf("Choose: ");
    scanf("%c%*c",&choose_char);
    while(choose_char != '1' && choose_char != '2' && choose_char != '3' && choose_char != '4')
    {
        printf("Choose error. Input again(1/2/3/4): ");
        scanf("%c%*c",&choose_char);
    }
    choose = choose_char - '0';
    switch(choose)
    {
        case 1:
            menuViewContact();
            break;
        case 2:
            menuViewFavorite();
            break;
        case 3:
            break;
        case 4:
            menuContactManager();
            break;
    }
}
void menuViewContact() {
    Res request, response;
    char contact_name[50];
    printf("Input contact name to search: ");
    scanf("%[^\n]%*c",contact_name);
    while(strlen(contact_name)==0)
    {
        printf("Contact name not valid, input again: ");
        scanf("%[^\n]%*c",contact_name);
    }
    request.code = VIEW_CONTACT;
    strcpy(request.value,contact_name);
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
void showContactViewed(Res response) {
    int contacts_Viewed;
    contacts_Viewed = atoi(getStrByIndex(response.value,0,"|"));
    for(int i=1; i <= contacts_Viewed; i++ )
    {
        printf("%s\n",getStrByIndex(response.value,i,"|"));
    }
}
void menuViewFavorite()
{
    Res request, response;
    printf("\n<<====View favorite====>>\n");
}
