#include "add_contact.h"

void inputAddContact(Res response) {
    Res request;
    char name[50], phone[12], email[30], address[100], group_name[30], note[500], favorite_char[4];
    char choose[10];
    int favorite, choose_int;
    int number_groups,i;
    int count=0;
    number_groups = atoi(getStrByIndex(response.value,0,"|"));

    request.code = REGISTER_NEW_CONTACT;
    printf("\t\t\tADD INFO CONTACT\n\n");
    printf("\t\t\tAdd name of contact: ");
    scanf("%[^\n]%*c",name);
    printf("\t\t\tAdd phone of contact: ");
    scanf("%[^\n]%*c",phone);
    printf("\t\t\tAdd email of contact: ");
    scanf("%[^\n]%*c",email);
    while(strstr(email,"@")==0 || strstr(email,".")==0) {
        printf("\t\t\tEmail not valid: ");
        scanf("%[^\n]%*c",email);
    }
    //printf("==>%s<",email);
    printf("\t\t\tAdd address of contact: ");
    scanf("%[^\n]%*c",address);
    if(number_groups!=0)
    {
        printf("\n\t\t\tList group name: \n");
        for(i = 1; i<=number_groups; i++)
        {
            printf("\t\t\t%d - %s\n",i, getStrByIndex(response.value,i,"|"));
        }
        printf("\t\t\t%d - No group \n",i);
        printf("\t\t\tChoose group number: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
        for(i=0 ;i <strlen(choose); i++)
        {
            if(choose[i]==',')
            count++;
        }
        if(count==0)
        {
            if(choose_int<=0 || choose_int > (number_groups+1))
            {
                printf("\t\t\tChoose error. Reinput:  ");
                menuActionContactViewed(response);
            }
            if(choose_int<=number_groups)
                sprintf(group_name,"%d-%s", 1, getStrByIndex(response.value,choose_int,"|"));
            else {
                sprintf(group_name,"%d",0);
            }
                //strcpy(group_name, getStrByIndex(response.value,choose_int,"|"));
        } else {
            sprintf(group_name,"%d",(count+1));
            for(int i=0 ; i <= count; i++)
            {
                strcat(group_name,"-");
                choose_int = atoi(getStrByIndex(choose,i,","));
                if(choose_int>number_groups)
                {
                    printf("Choose group error\n");
                    menuActionContactViewed(response);
                }
                strcat(group_name,getStrByIndex(response.value,choose_int,"|"));
            }
        }


    } else {
        //sprintf(group_name,"%d",0);
        strcpy(group_name,"0");
    }


    printf("\t\t\tFavorite(yes/no): ");
    scanf("%[^\n]%*c",favorite_char);
    while(strcmp(favorite_char,"yes")!=0 && strcmp(favorite_char,"no")!=0)
    {
        printf("\t\t\tChoose error. Choose again(yes/no):  ");
        scanf("%[^\n]%*c",favorite_char);
    }
    if(strcmp(favorite_char,"yes")==0)
        favorite = 1;
    else
        favorite = 0;
    printf("\t\t\tAdd Note of contact: ");

    scanf("%[^\n]%*c",note);
    sprintf(request.value,"%s|%s|%s|%s|%s|%d|%s",name,phone,email,address,group_name,favorite,note);
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}

