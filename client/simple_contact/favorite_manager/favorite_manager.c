#include "favorite_manager.h"
void menuFavoriteManager()
{
    char choose[10];
    int choose_int;
    printf("\n%s\t\t\t######### FAVORITE MANAGER #########\n",ANSI_COLOR_MAGENTA );
    printf("\t\t\t#  1. View favorite                #\n");
    printf("\t\t\t#  2. Delete contacts in favorite  #\n");
    printf("\t\t\t#  3. BACK                         #\n");
    printf("\t\t\t####################################%s\n",ANSI_COLOR_RESET );
    printf("\t\t\tChoose(1/2/3): ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3)
    {
        printf("\t\t\tChoose error. Choose again(1/2/3): ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    switch(choose_int)
    {
        case 1:
            viewFavorite();
            break;
        case 2:
            deleteContactInFavorite();
            break;
        case 3:
            menuContactManager();
            break;
        default:
            menuContactManager();
            break;
    }

}
void viewFavorite()
{
    Res request, response;
    request.code = VIEW_FAVORITE;
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
int showFavorite(Res response)
{
    int number_favorites;
    number_favorites = atoi(getStrByIndex(response.value,0,"|"));
    if(number_favorites==0)
    {
        printf("\n\t\t\tFavorite empty\n");
    } else
      {
        printf("\n\t\t\tLIST FAVORITE(%d):\n",number_favorites);
        for(int i = 1; i<= number_favorites; i++)
        {
            printf("\t\t\t%d - %s\n",i,getStrByIndex(response.value,i,"|"));
        }
        printf("\t\t\t+++++++++++++++++++++\n\n");

      }
    return number_favorites;
}
void deleteContactInFavorite()
{
    char choose[10];
    int choose_int, count, tmp;
    int number_favorites,i;
    char list_contacts[200], confirm[10];
    Res request, response, new_response;
    request.code = VIEW_FAVORITE;
    sendRequest(request);
    receiveResponse(&response);
    number_favorites = showFavorite(response);
    if(number_favorites==0) menuFavoriteManager();
    else
    {
        request.code =DELETE_CONTACTS_IN_FAVORITE;
        count = 0;
        printf("\t\t\tChoose contacts to delete from favorite(1,2..): ");
        scanf("%[^\n]%*c",choose);
        for(int i=0; i<strlen(choose); i++)
        {
            if(choose[i]==',') count++;
        }
        if(count==0)
        {
            choose_int = atoi(choose);
            if(choose_int<=0||choose_int>number_favorites) {
                printf("\t\t\tChoose error\n");
                menuFavoriteManager();
            } else {
                sprintf(request.value,"%d|%s",1,getStrByIndex(response.value,choose_int,"|"));
                //printf("Value====> %s\n",request.value);
                printf("\t\t\tDo you want to delete contact infavorite(yes/no)? ");
                scanf("%[^\n]%*c",confirm);
                if(strcmp(confirm,"yes")==0)
                {
                    sendRequest(request);
                    receiveResponse(&new_response);
                    processResponse(new_response);
                }
                else{
                    menuFavoriteManager();
                }


            }
        } else {
            tmp = 0;
            sprintf(request.value,"%d",count+1);
            for(i = 0; i<=count; i++)
            {
                tmp = atoi(getStrByIndex(choose,i,","));
                if(tmp<=number_favorites)
                {
                    strcat(request.value,"|");
                    strcat(request.value,getStrByIndex(response.value,tmp,"|"));
                } else {
                    printf("\t\t\tChoose error.\n");
                    break;
                }
            }
            if(i!=(count+1))
            {
                printf("\t\t\tError %d\n",i);
                menuFavoriteManager();
            }
            else
            {
                //printf("Value====> %s\n",request.value);
                printf("\t\t\tDo you want to delete contact infavorite(yes/no)? ");
                scanf("%[^\n]%*c",confirm);
                if(strcmp(confirm,"yes")==0)
                {
                    sendRequest(request);
                    receiveResponse(&new_response);
                    processResponse(new_response);
                }
                else{
                    menuFavoriteManager();
                }
                sendRequest(request);
                receiveResponse(&new_response);
                processResponse(new_response);
            }

        }
    }
}
