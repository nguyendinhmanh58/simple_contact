#ifndef FAVORITE_MANAGER_H
#define FAVORITE MANAGER_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"

void menuFavoriteManager();
void viewFavorite();
int showFavorite(Res response);
void deleteContactInFavorite();
#endif // FAVORITE_MANAGER_H
