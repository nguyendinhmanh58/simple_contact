#include "backup.h"
void backup()
{
    char choose[100];
    int choose_int;
    printf("\n%s\t\t\t############# BACKUP ##############\n",ANSI_COLOR_BLUE );
    printf("\t\t\t# 1. Choose name file to backup   #\n");
    printf("\t\t\t# 2. Backup automatic             #\n");
    printf("\t\t\t# 3. BACK                         #\n");
    printf("\t\t\t###################################%s\n",ANSI_COLOR_RESET );
    printf("\t\t\tChoose(1/2/3): ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3)
    {
        printf("\t\t\tChoose error. Choose again: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    switch(choose_int)
    {
        case 1:
            chooseNameFileBackup();
            break;
        case 2:
            backupAutomatic();
            break;
        case 3:
            menuContactManager();
            break;
        default:
            menuContactManager();
            break;
    }
}
void chooseNameFileBackup()
{
    Res request, response;
    char namefile[40];
    char choose[10];
    printf("\t\t\tInput name file to backup: ");
    scanf("%[^\n]%*c",namefile);
    while(strlen(namefile)==0)
    {
        printf("\t\t\tName file can't blank. Input again: ");
        scanf("%[^\n]%*c",namefile);
    }
    request.code = BACKUP;
    strcpy(request.value,namefile);
    //printf("request value: =====>%s\n",request.value);
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
    printf("\t\t\t");
    if(response.code == BACKUP_ERROR_FILE_ALREADY_EXISTED)
    {
        scanf("%[^\n]%*c",choose);
        while(strcmp(choose,"yes")!=0 && strcmp(choose,"no")!=0)
        {
            scanf("%[^\n]%*c",choose);
        }
        if(strcmp(choose,"yes")==0)
        {
            request.code = BACKUP_WITH_REPLACE_FILE_EXISTED;
            strcpy(request.value, namefile);
            sendRequest(request);
            receiveResponse(&response);
            processResponse(response);
        }
        else
        {
            backup();
        }
    }
}
void backupAutomatic()
{
    char confirm[10];
    Res request, response;
    request.code = BACKUP;
    printf("\t\t\tYou want to backup automatic(yes/no)? ");
    scanf("%[^\n]%*c",confirm);
    if(strcasecmp(confirm,"yes")==0)
    {
        sendRequest(request);
        receiveResponse(&response);
        processResponse(response);
    }
    else
        backup();

}
