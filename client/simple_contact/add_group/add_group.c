#include "add_group.h"
void addGroup()
{
    Res request, response;
    char group_name[30], desc[200];
    printf("\t\t\tInput group name: ");
    scanf("%[^\n]%*c",group_name);
    while(strlen(group_name)==0)
    {
        printf("\t\t\tGroup name can't blank. Reinput: \n");
        scanf("%[^\n]%*c",group_name);
    }
    printf("\t\t\tInput desc: ");
    scanf("%[^\n]%*c",desc);
    sprintf(request.value,"%s|%s",group_name,desc);
    request.code = ADD_GROUP;
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
