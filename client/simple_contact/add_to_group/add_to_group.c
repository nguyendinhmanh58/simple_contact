#include "add_to_group.h"

void getAllGroup(Res* response)
{
    Res request;
    request.code = GET_ALL_GROUPS;
    sendRequest(request);
    receiveResponse(response);
}
void add_to_group(Res new_response, Res response)
{
    int total_group;
    Res new_response1;
    int choose_int, count, choose_group;
    char choose[10], confirm[10];
    Res request;
    request.code = ADD_CONTACT_TO_GROUP;
    total_group = atoi(getStrByIndex(new_response.value,0,"|"));
    if(total_group==0)
    {
        printf("%s\n\t\t\tList group empty.\n",ANSI_COLOR_RED );
        menuActionContactViewed(response);
    }

    else if(total_group==1)
    {
        //strcpy(request.value,getStrByIndex(new_response.value,1,"|"));
        printf("\t\t\tYou only have one group is: %s\n", getStrByIndex(new_response.value,1,"|"));
        printf("\t\t\tDo you want to add this contact to group \"%s\"(yes/no): ", getStrByIndex(new_response.value,1,"|"));
        scanf("%[^\n]%*c", confirm);
        if(strcmp(confirm, "yes") == 0){
            sprintf(request.value,"%s|%s|%s",getStrByIndex(response.value,0,"|"),"1",getStrByIndex(new_response.value,1,"|"));
            sendRequest(request);
            receiveResponse(&new_response1);
            processResponse(new_response1);
            menuActionContactViewed(response);
        }
        else
            menuActionContactViewed(response);
    } else {
        for(int i = 1; i<= total_group; i++)
        {
            printf("\t\t\t%d. %s\n",i, getStrByIndex(new_response.value,i,"|"));
        }
        printf("\t\t\tChoose group: ");
        scanf("%[^\n]%*c",choose);
        for(int i = 0; i<strlen(choose); i++)
        {
            if(choose[i]==',')
                count++;
        }
        if(count==0)
        {
            choose_int = atoi(choose);
            while(choose_int <= 0 || choose_int > total_group)
            {
                printf("\t\t\tChoose error. input again: ");
                scanf("%[^\n]%*c",choose);
                choose_int = atoi(choose);
            }
            sprintf(request.value,"%s|%d|%s",getStrByIndex(response.value,0,"|"),1,getStrByIndex(new_response.value,choose_int,"|"));
        }
        else {
            sprintf(request.value,"%s|%d",getStrByIndex(response.value,0,"|"),count+1);
            for(int i=0 ; i <= count; i++)
            {
                strcat(request.value,"|");
                choose_group = atoi(getStrByIndex(choose,i,","));
                strcat(request.value,getStrByIndex(new_response.value,choose_group,"|"));
            }
        }

        //strcpy(request.value,getStrByIndex(new_response.value,choose_int,"|"));
        sendRequest(request);
        receiveResponse(&new_response1);
        processResponse(new_response1);
        //printf("response=>%s<",response.value);
        menuActionContactViewed(response);
    }

}
