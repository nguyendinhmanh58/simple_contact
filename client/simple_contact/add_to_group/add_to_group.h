#ifndef ADD_TO_GROUP_H
#define ADD_TO_GROUP_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"
#include "./../../../helper/functions.h"

void add_to_group(Res new_response, Res response);
void getAllGroup(Res* response);


#endif // ADD_TO_GROUP_H
