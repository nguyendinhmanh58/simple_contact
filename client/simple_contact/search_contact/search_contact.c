#include "search_contact.h"
void menu_search_contact() {
    char choose[10];
    int choose_int;
  printf( "\n\t\t\t###### SEARCH CONTACT ######\n");
    printf("\t\t\t#     1. Search            #\n");
    printf("\t\t\t#     2. View all contact  #\n");
    printf("\t\t\t#     3. Back              #\n");
    printf("\t\t\t############################\n");
    printf("\t\t\tYou choose: ");
    scanf("%[^\n]%*c",choose);
    while(strcmp(choose,"1")!=0 && strcmp(choose,"2")!=0 && strcmp(choose,"3")!=0)
    {
        printf("\n\t\t\tChoose error. Choose again: ");
        scanf("%[^\n]%*c",choose);
    }
    choose_int = atoi(choose);
    switch(choose_int)
    {
        case 1:
            search_contact();
            break;
        case 2:
            viewAllContact();
            break;
        case 3:
            menuContactManager();
            break;

    }

}
void viewAllContact()
{
    Res request, response;
    request.code = SEARCH_CONTACTS;
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
void search_contact() {
    Res request, response;
    char contact_search[50];
    printf("\t\t\tInput value to search contact: ");
    scanf("%[^\n]%*c",contact_search);
    while(strlen(contact_search)==0)
    {
        printf("\t\t\tValue is not valid. Input again: ");
        scanf("%[^\n]%*c",contact_search);
    }
    request.code = SEARCH_CONTACTS;
    strcpy(request.value,contact_search);
    sendRequest(request);
    receiveResponse(&response);
    processResponse(response);
}
void showListContactToView(Res response) {
    Res request, response1;
    int number_contact_to_view = 0;
    char choose_contact[10];
    int choose_contact_int;
    number_contact_to_view = atoi(getStrByIndex(response.value,0,"|"));
    if(number_contact_to_view==1)
    {
        request.code = VIEW_CONTACT;
        strcpy(request.value,getStrByIndex(response.value,1,"|"));
        sendRequest(request);
        receiveResponse(&response1);
        processResponse(response1);
    } else {
        printf("\t\t\tList contact from server: \n");
        for(int i=1;i<=number_contact_to_view;i++)
        {
            printf("\t\t\t%d. %s\n",i, getStrByIndex(response.value,i,"|"));
        }
        printf("\t\t\tChoose contact to view/favorite/update(1->%d):",number_contact_to_view);
        scanf("%[^\n]%*c",choose_contact);
        choose_contact_int = atoi(choose_contact);
        while(choose_contact_int <= 0 || choose_contact_int > number_contact_to_view)
        {
            printf("\t\t\tChoose error. Input again: ");
            scanf("%[^\n]%*c",choose_contact);
            choose_contact_int = atoi(choose_contact);
        }
        request.code = VIEW_CONTACT;
        strcpy(request.value, getStrByIndex(response.value,choose_contact_int,"|"));
        sendRequest(request);
        receiveResponse(&response1);
        processResponse(response1);
    }
}
void showContactViewed(Res response)
{
    char s[50];
    char group_names[256];
    int total_groups;
    //printf("\t\t\t##########################################\n");
    printf("\t\t\t ________________________________________\n");
    printf("\t\t\t|%-12s%28s|\n","Contact Name", getStrByIndex(response.value,0,"|"));
    printf("\t\t\t|%-12s%28s|\n","Phone", getStrByIndex(response.value,1,"|"));
    printf("\t\t\t|%-12s%28s|\n","Email", getStrByIndex(response.value,2,"|"));
    printf("\t\t\t|%-12s%28s|\n","Address", getStrByIndex(response.value,3,"|"));
    strcpy(group_names,getStrByIndex(response.value,4,"|"));
    total_groups = atoi(getStrByIndex(group_names,0,"-"));
    printf("\t\t\t|%-s(%d)%33s\n","Group",total_groups,"|");
    for(int i = 1; i<total_groups; i++){
        printf("\t\t\t|%40s|\n",getStrByIndex(group_names,i,"-"));
    }
    printf("\t\t\t|%40s|\n",getStrByIndex(group_names,total_groups,"-"));

    if(strcmp(getStrByIndex(response.value,5,"|"),"1")==0)
        printf("\t\t\t|%-12s%28s|\n","Favorite", "yes");
    else
        printf("\t\t\t|%-12s%28s|\n","Favorite", "no");
    printf("\t\t\t|%-12s%27s\n","Note", getStrByIndex(response.value,6,"|"));
    //printf("\t\t\t##########################################\n");
    //printf("\t\t\t|________________________________________|\n");

}
void menuActionContactViewed(Res response) {
    char choose[10], confirm[10];
    int choose_int;
    char contact_name[50];
    Res request, new_response;
    printf("\n%s\t\t\t############################\n",ANSI_COLOR_CYAN );
  printf("\t\t\t#   1. Add favorite        #\n");
    printf("\t\t\t#   2. Add to group        #\n");
    printf("\t\t\t#   3. Update this contact #\n");
    printf("\t\t\t#   4. Delete this contact #\n");
    printf("\t\t\t#   5. Back                #\n");
    printf("\t\t\t############################%s\n",ANSI_COLOR_RESET );
    printf("\t\t\tYou choose: ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3 && choose_int!=4 && choose_int!=5)
    {
        printf("\t\t\tChoose error. input again: ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    strcpy(contact_name,getStrByIndex(response.value,0,"|"));
    switch(choose_int)
    {
        case 1:
            request.code = ADD_FAVORITE;
            strcpy(request.value,contact_name);
            sendRequest(request);
            receiveResponse(&new_response);
            //printf("response===> %s<\n",response.value);
            processResponse(new_response);
            menuActionContactViewed(response);
            break;
        case 2:
            getAllGroup(&new_response);
            processResponse(new_response);
            //printf("response===> %s<\n",response.value);
            add_to_group(new_response, response);
            break;
        case 3:
            update_contact(response);
            break;
        case 4:
            printf("\t\t\tDo you want to delete this contact(yes/no)? ");
            scanf("%[^\n]%*c",confirm);
            if(strcmp(confirm,"yes")==0)
                delete_contact(response);
            else
                menuActionContactViewed(response);
            break;
        case 5:
            menu_search_contact();
            break;
    }
}
