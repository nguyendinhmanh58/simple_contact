#ifndef SEARCH_CONTACT_H
#define SEARCH_CONTACT_H

#include "./../../libs/client_lib.h"
#include "./../../../libs/typedef.h"
#include "./../add_to_group/add_to_group.h"
#include "./../update_contact/update_contact.h"
#include "./../delete_contact/delete_contact.h"

void search_contact();
void menu_search_contact();
void showListContactToView(Res response);
void showContactViewed(Res response);
void menuActionContactViewed(Res response);
void viewAllContact();

#endif // SEARCH_CONTACT_H
