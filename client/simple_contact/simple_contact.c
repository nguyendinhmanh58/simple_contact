#include "simple_contact.h"
void menuContactManager(){
    char choose[10], old_pass[30];
    int tmp, choose_int;
    Res request, response;
  printf(ANSI_COLOR_GREEN "\n\t\t\t##########################\n");
    printf("\t\t\t#  Welcome to my contact #\n");
    printf("\t\t\t#                        #\n");
    printf("\t\t\t#  1. Add contact        #\n");
    printf("\t\t\t#  2. Add group          #\n");
    printf("\t\t\t#  3. View contact       #\n");
    printf("\t\t\t#  4. View group         #\n");
    printf("\t\t\t#  5. Favorite manager   #\n");
    printf("\t\t\t#  6. Backup             #\n");
    printf("\t\t\t#  7. Restore            #\n");
    printf("\t\t\t#  8. Change password    #\n");
    printf("\t\t\t#  9. LOGOUT             #\n");
    printf("\t\t\t##########################\n" ANSI_COLOR_RESET);
    printf("\t\t\t   You choose:  ");
    scanf("%[^\n]%*c",choose);
    choose_int = atoi(choose);
    while(choose_int!=1 && choose_int!=2 && choose_int!=3 && choose_int!=4 && choose_int!=5 && choose_int!=6 && choose_int!=7 && choose_int!=8 && choose_int !=9){
        printf("\t\t\t   Your choose do not exist. Try again\n");
        printf("\t\t\t   Choose(1/2/3/4/5/6/7/8): ");
        scanf("%[^\n]%*c",choose);
        choose_int = atoi(choose);
    }
    switch(choose_int)
    {
        case 1:
            request.code = ADD_CONTACT;
            sendRequest(request);
            receiveResponse(&response);
            processResponse(response);
            break;
        case  2:
            addGroup();
            break;
        case 3:
            menu_search_contact();
            break;
        case 4:
            menuSearchGroup();
            break;
        case 5:
            menuFavoriteManager();
            break;
        case 6:
            backup();
            break;
        case 7:
            restore();
            break;
        case 8:
            printf("\t\t\tInput your old password: ");
            getPassword(old_pass);
            request.code = RESET_PASSWORD;
            strcpy(request.value, str2md5(old_pass));
            sendRequest(request);
            receiveResponse(&response);
            processResponse(response);
            break;
        case 9:
            //logout
            logout();
            break;
        default:
            printf("Error choose\n");
            menuStart();
            break;
    }
}
