#ifndef FORGOT_PASSWORD_H
#define FORGOT_PASSWORD_H
#include "./../../libs/typedef.h"
void ProcessForgotPass(Res request, int i, ClientInfo clients[], Res *response, SecretQuestion* secret_questions);
void ProcessAnswerQuestion(Res request, int index, ClientInfo clients[], Res *response);
void ProcessRegisterNewPass(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount);

#endif
