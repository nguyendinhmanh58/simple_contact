#include "forgot-password.h"
void ProcessForgotPass(Res request, int index, ClientInfo clients[], Res *response, SecretQuestion* secret_questions){
    char buff[10];
    if(clients[index].status !=  SPECIFIED_ID)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be SPECIFIED_ID");
    }
    response->code = FORGOT_PASS_OK;
    sprintf(response->value, "%d|", NUMBER_SECRET_QUESTION);
    for(int i = 0; i  < NUMBER_SECRET_QUESTION; i++){
        sprintf(buff,"%d",secret_questions[i].question_id);
        strcat(response->value, buff);
        strcat(response->value, "-");
        strcat(response->value, secret_questions[i].question);
        strcat(response->value, "|");
    }
    clients[index].status = FORGOT_PASSWORD;
}

void ProcessAnswerQuestion(Res request, int index, ClientInfo clients[], Res *response){
    char str[100];
    int question_id;
    char answer[50];
    if(clients[index].status !=  FORGOT_PASSWORD)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be FORGOT_PASSWORD");
    }

    strcpy(str, getStrByIndex(request.value, 0, "-"));
    question_id = atoi(str);
    strcpy(answer, getStrByIndex(request.value, 1, "-"));
    for(int i = 0; i < NUMBER_SECRET_QUESTION; i++){
        if(clients[index].current_account.secret_questions[i].question_id == question_id){
            if(strcmp(clients[index].current_account.secret_questions[i].answer, answer) == 0){
                if(i == (NUMBER_SECRET_QUESTION - 1)){
                    response->code = INPUT_NEW_PASS;
                    clients[index].status = CHANGE_PASSWORD;
                    return;
                } else {
                    response->code = CORRECT_ANSWER;
                    sprintf(response->value, "%sYour answer is correct!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
                    return;
                }
            }
        }
    }
    response->code = ERROR_ANSWER;
    // update status for clients back to menu Login
    clients[index].status = UNAUTHENTICATED;
    sprintf(response->value, "%sError. Your answer not match!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
}

void ProcessRegisterNewPass(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount){
    SllistNode* node;
    if(clients[index].status !=  CHANGE_PASSWORD)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be CHANGE_PASSWORD");
    }

    for(node = listAccount->root; node != NULL; node = node->next){
        if(strcmp(node->element.username, clients[index].current_account.username) == 0){
            strcpy(clients[index].current_account.password, request.value);
            strcpy(node->element.password, request.value);
            saveAccountsInfo(listAccount, "server/account/account.txt");
            response->code = CHANGE_PASS_OK;
            sprintf(response->value, "%sChange pass success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
            // reset UNAUTHENTICATED
            clients[index].status = UNAUTHENTICATED;
            return;
        }
    }
}

