#ifndef LOGIN_H
#define LOGIN_H
#include "./../../libs/typedef.h"
void ProcessLoginUser(Res request, int index, ClientInfo clients[], SllistType *listAccount,  Res* response);
void ProcessLoginPass(Res request, int index, ClientInfo clients[], Res* response, SllistType* listAccount);
void ProcessReLogin(ClientInfo clients[], int index, Res* response);
#endif // LOGIN_H
