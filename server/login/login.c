#include "login.h"
void ProcessLoginUser(Res request, int index,ClientInfo clients[], SllistType *listAccount, Res *response)
{
    elementtype e;
    SllistNode* node;
    /*check username is exist*/
    strcpy(e.username, request.value);
    node = searchFunc(listAccount, e);
    if(node != NULL)
    {
        int current_time = (int)time(NULL);
        if(node->element.time_lock != -1
            && current_time - node->element.time_lock < TIME_LOG
            && (clients[index].status == UNAUTHENTICATED  || clients[index].status == BLOCKING))
        {
            response->code = BLOCK;
            sprintf(response->value, "%sSorry. Your account is locked!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
            if(clients[index].status == UNAUTHENTICATED) clients[index].status = BLOCKING;
            return;
        }
        else
        {
            /* reset status if time log > 15 minutes*/
            if(clients[index].status == BLOCKING) clients[index].status = UNAUTHENTICATED;

            /*check status must be UNAUTHENTICATED*/
            if(clients[index].status != UNAUTHENTICATED)
            {
                response->code = STATUS_FAILURE;
                strcpy(response->value, "Account status must be unthenticated");
                return;
            }
            response->code = LOGIN_USER_OK;
            strcpy(response->value, "");
            clients[index].status = SPECIFIED_ID;
            clients[index].current_account = (Account)node->element;
            return;
        }
    }
    else
    {
        response->code = LOGIN_USER_ERROR;
        sprintf(response->value, "%sError. Username do not exist!\n%s",ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
}

void ProcessLoginPass(Res request, int index,ClientInfo clients[], Res *response, SllistType* listAccount)
{
    SllistNode *node;
    char contactFileName[200];
    /*check status must be UNAUTHENTICATED*/
    if(clients[index].status != SPECIFIED_ID)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be specified_id");
        return;
    }

    /*check username is exist*/
    if(strcmp(clients[index].current_account.password, request.value) == 0)
    {
        response->code = LOGIN_PASS_OK;
        sprintf(response->value,"%sLogin success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
        clients[index].status = AUTHENTICATED;
        clients[index].password_fail_times = 0;
        getLinkFileContactsData(clients[index].current_account.username, contactFileName);
        readContactsInfo(&clients[index], contactFileName);
        return;
    }
    else
    {
        if(clients[index].password_fail_times >= 3)
        {
            response->code = BLOCK;
            sprintf(response->value, "%sYou enter the wrong pass more than 3 times. Your account will be locked by 15 minutes!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
            clients[index].current_account.time_lock = (int)time(NULL);
            node = searchFunc(listAccount, clients[index].current_account);
            node->element.time_lock = (int)time(NULL);
            saveAccountsInfo(listAccount, "server/account/account.txt");
            clients[index].status = BLOCKING;
            return;
        }
        else
        {
            response->code = LOGIN_PASS_ERROR;
            sprintf(response->value, "%sError. Password is invalid!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
            clients[index].password_fail_times ++;
            return;
        }
    }
}


void ProcessReLogin(ClientInfo clients[], int index, Res* response){
    clients[index].status = UNAUTHENTICATED;
    response->code = RE_LOGIN_OK;
}

