#ifndef BACKUP_H
#define BACKUP_H
#include "./../../libs/typedef.h"
void getLinkFileBackupAuto(char* username, char* linkFile, char *fileName);
void ProcessBackup(Res request, int index, ClientInfo clients[],Res* response, int replace_flag);
#endif // BACKUP_H
