#include "backup.h"
#include <time.h>
void getLinkFileBackupAuto(char* username, char* linkFile, char *fileName){
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time[30];
    strcpy(linkFile, "server/users/");
    strcat(linkFile, username);
    strcat(linkFile, "/");
    strcpy(fileName, "backup_");
    sprintf(time, "%d-%d-%d_%d:%d:%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    strcat(fileName, time);
    strcat(fileName, ".txt");
    strcat(linkFile, fileName);
}

void ProcessBackup(Res request, int index, ClientInfo clients[],Res* response, int replace_flag){
    FILE *fp_check, *fp_backup;
    char linkFile[200], fileName[100], str[200];
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    if(strcmp(request.value, "") != 0){
        sprintf(linkFile, "server/users/%s/%s", clients[index].current_account.username, request.value);
        fp_check = fopen(linkFile, "r");
        if(fp_check != NULL){
            if(strcmp(request.value, "data.txt") == 0){
                response->code  = BACKUP_ERROR_CANNOT_SET_NAME_COINCIDE_WITH_FILE_DATA;
                sprintf(response->value, "%sError. Canot set file backup with name \"data.txt\"%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
                return;
            } else if(replace_flag == 0){
                response->code = BACKUP_ERROR_FILE_ALREADY_EXISTED;
                strcpy(response->value, "File already existed. Do you want replace this file on server?(yes/no): ");
                return;
            }
        }
        if(fp_check == NULL || replace_flag == 1){
            fp_backup = fopen(linkFile, "w");
            if(fp_backup == NULL){
                response->code = BACKUP_ERROR_CANNOT_MAKE_FILE;
                sprintf(response->value, "%sError. Can not make file backup. Has some error in serer\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
                return;
            }
            saveContactsInfo(clients[index], linkFile);
            response->code = BACKUP_OK;
            sprintf(response->value, "%sBackup success!!!%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
        }
    } else {
        getLinkFileBackupAuto(clients[index].current_account.username, linkFile, fileName);
        fp_backup = fopen(linkFile, "w");
        if(fp_backup == NULL){
            response->code = BACKUP_ERROR_CANNOT_MAKE_FILE;
            sprintf(response->value, "%sError. Can not make file backup. Has some error in serer\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
            return;
        }
        saveContactsInfo(clients[index], linkFile);
        response->code = BACKUP_OK;
        sprintf(str, "%sBackup success! File backup with name \"%s\" has been saved on server!\n%s", ANSI_COLOR_BLUE, fileName, ANSI_COLOR_RESET);
        strcpy(response->value, str);
    }
}
