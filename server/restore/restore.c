#include "restore.h"
#include <dirent.h>
void ProcessGetAllFiles(Res request, int index, ClientInfo clients[], Res *response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    DIR *d;
    struct dirent *dir;
    int number_files;
    char fileNames[1024] = "";
    char dirName[200] = "";
    number_files = 0;
    sprintf(dirName, "./server/users/%s/", clients[index].current_account.username);
    d = opendir(dirName);
    if(d)
    {
        while ((dir = readdir(d)) != NULL){
            if(strcmp(dir->d_name, "data.txt") != 0 && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0) {
                number_files++;
                strcat(fileNames, "|");
                strcat(fileNames, dir->d_name);
            }
        }
        closedir(d);
    }
    if(number_files == 0){
        response->code = ERROR_NO_FILE;
        sprintf(response->value, "%sError. You do not have any backup file!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
    response->code = ALL_FILES;
    sprintf(response->value, "%d%s", number_files,fileNames);
}

void ProcessRestore(Res request, int index, ClientInfo clients[], Res *response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    char linkFile[200], fileData[200];
    FILE *fp;
    sprintf(linkFile, "server/users/%s/%s", clients[index].current_account.username, request.value);
    fp = fopen(linkFile, "r");
    if(fp == NULL){
        response->code = FILE_NOT_EXIST;
        sprintf(response->value, "%sError. File backup \"%s\" do not exits!\n%s",ANSI_COLOR_RED, request.value,  ANSI_COLOR_RESET);
        return;
    }
    close(fp);
    readContactsInfo(&clients[index], linkFile);
    getLinkFileContactsData(clients[index].current_account.username, fileData);
    saveContactsInfo(clients[index], fileData);
    response->code = RESTORE_OK;
    sprintf(response->value, "%sRestore success!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}
