#include "signup.h"
void ProcessSign(Res request, int index, ClientInfo clients[], Res *response)
{
    /*check status must be UNAUTHENTICATED*/
    if(clients[index].status != UNAUTHENTICATED)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be unthenticated");
        return;
    }
    else
    {
        response->code = SIGN;
        clients[index].status = SIGN_USER;
        return;
    }
}


void ProcessSignUserId(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount, SecretQuestion* secret_questions)
{
    elementtype e;
    SllistNode* node;
    char buff[10];
    /*check status must be SIGN_USER*/
    if(clients[index].status != SIGN_USER)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be SIGN_USER");
        return;
    }
    /*check username is exist*/
    strcpy(e.username, request.value);
    node = searchFunc(listAccount, e);
    if(node != NULL)
    {
        response->code = ERROR_SIGN_USER_EXIST;
        sprintf(response->value, "%sError. Username is existed!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        // change status client -> UNAUTHENTICATED
        clients[index].status = UNAUTHENTICATED;
        return;
    }
    else
    {
        response->code = SIGN_USER_OK;
        //strcpy(response->value, "");
        sprintf(response->value, "%d|", NUMBER_SECRET_QUESTION);
        for(int i = 0; i < NUMBER_SECRET_QUESTION; i++){
            sprintf(buff,"%d",secret_questions[i].question_id);
            strcat(response->value, buff);
            strcat(response->value, "-");
            strcat(response->value, secret_questions[i].question);
            strcat(response->value, "|");
        }
        clients[index].status = SIGN_PASS_AND_OHTER_INFO;
        return;
    }
}


void ProcessSignInfo(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount)
{
    elementtype e;
    SllistNode* node;
    int strtok_index = 0;
    char command[100];
    /*check status must be SIGN_USER*/
    if(clients[index].status != SIGN_PASS_AND_OHTER_INFO)
    {
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be SIGN_PASS_AND_OHTER_INFO");
        return;
    }
    /*check username is exist*/
    // <username>|<password>|<firsname>|<lastname>|<question_id>|<answer>|
    strcpy(e.username, getStrByIndex(request.value, strtok_index++, "|"));
    node = searchFunc(listAccount, e);
    if(node != NULL)
    {
        response->code = ERROR_SIGN_USER_EXIST;
        sprintf(response->value, "%sError. Username is existed!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        // change status of client to AUTHENTICATED
        clients[index].status = AUTHENTICATED;
        return;
    }
    else
    {
        strcpy(e.password, getStrByIndex(request.value, strtok_index++, "|"));
        strcpy(e.firstname, getStrByIndex(request.value, strtok_index++, "|"));
        strcpy(e.lastname, getStrByIndex(request.value, strtok_index++, "|"));
        e.time_lock = -1;
        for(int i = 0; i< NUMBER_SECRET_QUESTION; i++){
            e.secret_questions[i].question_id = atoi(getStrByIndex(request.value, strtok_index++, "|"));
            strcpy(e.secret_questions[i].answer, getStrByIndex(request.value, strtok_index++, "|"));
        }
        insertAtEndSllist(listAccount, e);
        saveAccountsInfo(listAccount, "server/account/account.txt");
        response->code = SIGN_OK;
        sprintf(response->value, "%sSignup success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
        // create directory for user
        strcpy(command, "mkdir server/users/");
        strcat(command, e.username);
        system(command);
        //strcpy(response->value, "");
        clients[index].status = UNAUTHENTICATED;
        return;
    }
}


