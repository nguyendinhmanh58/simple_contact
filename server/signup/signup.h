#ifndef SIGNUP_H
#define SIGNUP_H
#include "./../../libs/typedef.h"
void ProcessSign(Res request, int index, ClientInfo clients[], Res *response);
void ProcessSignUserId(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount, SecretQuestion* secret_questions);
void ProcessSignInfo(Res request, int index, ClientInfo clients[], Res *response, SllistType* listAccount);
#endif // SIGNUP_H
