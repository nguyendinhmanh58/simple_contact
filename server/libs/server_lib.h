#ifndef SERVER_LIB_H
#define SERVER_LIB_H
#include "./../../libs/typedef.h"
#include "./../../libs/define.h"

void init();
bool readAccountsInfo(SllistType* listAccount,char *fileName);
void saveAccountsInfo(SllistType* listAccount,char* fileName);
void readContactsInfo(ClientInfo* client, char *contactFileName);
void saveContactsInfo(ClientInfo client, char* fileName);
void getLinkFileContactsData(char *username, char *linkFile);
void logRes(Res res);
void logStatus(ClientInfo client);
void resetResponse(Res* response);
#endif // SERVER_LIB_H
