#include <stdio.h>

#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include "server_lib.h"
#include "./../../libs/btree/btree.h"
#include "./../../libs/dllist/dllist.h"

void init(SecretQuestion* secret_questions)
{
    int i;
    for(i = 0; i < NUMBER_SECRET_QUESTION; i++)
    {
        secret_questions[i].question_id = i;
        switch(i)
        {
        case 0:
            strcpy(secret_questions[i].question,"What is your pet’s name?");
            break;
        case 1:
            strcpy(secret_questions[i].question, "What is the first name of the person you first kissed?");
            break;
        case 2:
            strcpy(secret_questions[i].question, "What time of the day were you born?");
            break;
        default:
            break;
        }
    }
}

bool readAccountsInfo(SllistType* listAccount,char *fileName){
    struct stat st;
    elementtype *e;
    char s[200], str[100];
    FILE *f;
    stat(fileName, &st);
    if (st.st_size != 0){
        // check file account.txt is not empty
        if((f=fopen(fileName,"r"))==NULL)
        {
            printf("Error. Khong ton tai file %s\n", fileName);
            return false;
        }
        while(fscanf(f, "%[^\n]\n", s)!=NULL)
        {
            if(strcmp(s,"") == 0) break;
            e = (elementtype *)malloc(sizeof(elementtype));
            strcpy(e->username,getStrByIndex(s, 0, "|"));
            strcpy(e->password,getStrByIndex(s, 1, "|"));
            strcpy(e->firstname,getStrByIndex(s, 2, "|"));
            strcpy(e->lastname,getStrByIndex(s, 3, "|"));
            e->time_lock = atoi(getStrByIndex(s, 4, "|"));
            for(int i = 0; i < NUMBER_SECRET_QUESTION; i++){
                strcpy(str,getStrByIndex(s, i+5, "|"));
                e->secret_questions[i].question_id = atoi(getStrByIndex(str, 0, "-"));
                strcpy(e->secret_questions[i].answer,getStrByIndex(str, 1, "-"));
            }
            insertAtEndSllist(listAccount, *e);
            if(feof(f))
            {
                break;
            }
        }
        fclose(f);
        return true;
    }
}

void saveAccountsInfo(SllistType* listAccount,char* fileName)
{
    FILE* fp;
    fp = fopen(fileName, "w");
    SllistNode * Node;
    for(Node = listAccount->root; Node!=NULL; Node = Node->next)
    {
        fprintf(fp, "%s|%s|%s|%s|%d",Node->element.username, Node->element.password, Node->element.firstname, Node->element.lastname, Node->element.time_lock);
        for(int i =0; i<NUMBER_SECRET_QUESTION; i++)
        {
            fprintf(fp, "|%d-%s", Node->element.secret_questions[i].question_id, Node->element.secret_questions[i].answer);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}

void readContactsInfo(ClientInfo *client, char *contactFileName){
    char s[1024], str[100], group_names[200];
    int number_groups, i;
    struct stat st;
    bTreeElementtype* be;
    DllistElementtype *de, *group;
    FILE *f;
    makeTree(&(client->contacts));
    client->groups = makeDllist();
    if((f=fopen(contactFileName,"r"))==NULL)
    {
        return;
    }
    stat(contactFileName, &st);
    if (st.st_size != 0){
        // check file data.txt is not empty
        while(fscanf(f, "%[^\n]\n", s)!=NULL){
            if(strcmp(s,"") == 0) break;
            /*
             *  format of file data.txt
             *  GROUP|<name>|<desc>
             *  CONTACT|<name>|<phone>|<email>|<address>|<group_name>|<favorite>|<note>
            */
            // check type of data CONTACT or GROUP
            strcpy(str, getStrByIndex(s, 0, "|"));
            if(strcmp(str, "CONTACT") == 0){
                // add contact to list<Btree>
                be = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
                strcpy(be->name, getStrByIndex(s, 1, "|"));
                strcpy(be->phone, getStrByIndex(s, 2, "|"));
                strcpy(be->email, getStrByIndex(s, 3, "|"));
                strcpy(be->address, getStrByIndex(s, 4, "|"));
                be->group_names = makeDllist();
                strcpy(group_names,getStrByIndex(s, 5, "|"));
                number_groups = atoi(getStrByIndex(group_names, 0, "-"));
                if(number_groups > 0){
                    for(i = 0; i < number_groups; i++){
                        group = (DllistElementtype *)malloc(sizeof(DllistElementtype));
                        strcpy(group->name, getStrByIndex(group_names, i+1, "-"));
                        insertAtEndDllist(be->group_names, *group);
                    }
                }
                be->favorite = atoi(getStrByIndex(s, 6, "|"));
                strcpy(be->note, getStrByIndex(s, 7, "|"));
                bInsert(&(client->contacts), *be);
            }
            if(strcmp(str, "GROUP")  == 0){
                de = (DllistElementtype *)malloc(sizeof(DllistElementtype));
                strcpy(de->name, getStrByIndex(s, 1, "|"));
                strcpy(de->desc, getStrByIndex(s, 2, "|"));
                insertAtEndDllist(client->groups, *de);
            }
            if(feof(f))
            {
                break;
            }
        }
        fclose(f);
    }
}


void saveContactsInfo(ClientInfo client, char* fileName){
    DllistNode* groupNode;
    FILE *fp;
    fp = fopen(fileName, "w");
    if(fp == NULL){
        return; // ERROR. Can not open file to write
    }
    if(client.groups != NULL){
        for(groupNode = client.groups->head; groupNode!= NULL; groupNode = groupNode->next){
            fprintf(fp, "GROUP|%s|%s\n", groupNode->element.name, groupNode->element.desc);
        }
    }
    inOrderSaveContacts(client.contacts, fp);
    fclose(fp);
}

//save theo thứ tự tăng dần
void inOrderSaveContacts(TreeType root, FILE* fp)
{
  // *  CONTACT|<name>|<phone>|<email>|<address>|<number_groups>-<group_name1>-<group_name2>|<favorite>|<note>
  if (root == NULL) return;
  inOrderSaveContacts(root->left, fp);
  fprintf(fp, "CONTACT|%s|%s|%s|%s",
    root->element.name,
    root->element.phone,
    root->element.email,
    root->element.address);
  int i, number_groups;
  DllistNode* group;
  number_groups = numberNodeDllist(root->element.group_names);
  //
  fprintf(fp, "|%d", number_groups);
  if(number_groups > 0){
        for(group = root->element.group_names->head; group != NULL;  group = group->next){
            fprintf(fp, "-%s", group->element.name);
        }
  }
  fprintf(fp, "|%d|%s\n",
    root->element.favorite,
    root->element.note);
  inOrderSaveContacts(root->right, fp);
}

void getLinkFileContactsData(char *username, char *linkFile){
    strcpy(linkFile, "server/users/");
    strcat(linkFile, username);
    strcat(linkFile, "/data.txt");
}

void logRes(Res res){
    switch(res.code){
        /* code for request */
        case LOGIN_USER:
            printf(" LOGIN_USER|");
            break;
        case LOGIN_PASS:
            printf("LOGIN_PASS|");
            break;
        case SIGN:
            printf("SIGN|");
            break;
        case SIGN_USER_ID:
            printf("SIGN_USER_ID|");
            break;
        case SIGN_INFO:
            printf("SIGN_INFO|");
            break;
        case FORGOT_PASS:
            printf("FORGOT_PASS|");
            break;
        case ANSWER_QUESTION:
            printf("ANSWER_QUESTION|");
            break;
        case REGISTER_NEW_PASS:
            printf("REGISTER_NEW_PASS|");
            break;
        case INPUT_NEW_PASS:
            printf("INPUT_NEW_PASS|");
            break;

        case SEARCH_CONTACTS:
            printf("SEARCH_CONTACTS|");
            break;
        case VIEW_CONTACT:
            printf("VIEW_CONTACT|");
            break;
        case VIEW_FAVORITE:
            printf("VIEW_FAVORITE|");
            break;
        case VIEW_GROUP:
            printf("VIEW_GROUP|");
            break;

        case ADD_CONTACT:
            printf("ADD_CONTACT|");
            break;
        case REGISTER_NEW_CONTACT:
            printf("REGISTER_NEW_CONTACT|");
            break;
        case ADD_GROUP:
            printf("ADD_GROUP|");
            break;
        case ADD_FAVORITE:
            printf("ADD_FAVORIT|");
            break;

        case ADD_CONTACT_TO_GROUP:
            printf("ADD_CONTACT_TO_GROUP|");
            break;

        case GET_ALL_GROUPS:
            printf("GET_ALL_GROUPS|");
            break;

        case UPDATE_CONTACT:
            printf("UPDATE_CONTACT|");
            break;
        case UPDATE_GROUP:
            printf("UPDATE_GROUP|");
            break;

        case DELETE_CONTACT:
            printf("DELETE_CONTACT|");
            break;
        case REMOVE_CONTACTS_FROM_GROUP:
            printf("REMOVE_CONTACTS_FROM_GROUP|");
            break;
        case DELETE_GROUP:
            printf("DELETE_GROUP|");
            break;

        case BACKUP:
            printf("BACKUP|");
            break;
        case BACKUP_WITH_REPLACE_FILE_EXISTED:
            printf("BACKUP_WITH_REPLACE_FILE_EXISTED|");
            break;
        case RESTORE:
            printf("RESTORE|");
            break;


        /* code for respone */
        case LOGIN_USER_OK:
            printf("LOGIN_USER_OK|");
            break;
        case LOGIN_USER_ERROR:
            printf("LOGIN_USER_ERROR|");
            break;
        case LOGIN_PASS_OK:
            printf("LOGIN_PASS_OK|");
            break;
        case LOGIN_PASS_ERROR:
            printf("LOGIN_PASS_ERROR|");
            break;
        case BLOCK:
            printf("BLOCK|");
            break;

        case SIGN_USER_OK:
            printf("SIGN_USER_OK|");
            break;
        case ERROR_SIGN_USER_EXIST:
            printf("ERROR_SIGN_USER_EXIST|");
            break;
        case ERROR_SIGN_PASS_INVALID:
            printf("ERROR_SIGN_PASS_INVALID|");
            break;
        case SIGN_OK:
            printf("SIGN_OK|");
            break;

        case FORGOT_PASS_OK:
            printf("FORGOT_PASS_OK|");
            break;
        case CORRECT_ANSWER:
            printf("CORRECT_ANSWER|");
            break;
        case ERROR_ANSWER:
            printf("ERROR_ANSWER|");
            break;
        case CHANGE_PASS_OK:
            printf("CHANGE_PASS_OK|");
            break;

        case FOUNDED_CONTACTS:
            printf("FOUNDED_CONTACTS|");
            break;
        case FOUNDED_CONTACT:
            printf("FOUNDED_CONTACT|");
            break;
        case NOT_FOUND_CONTACT:
            printf("NOT_FOUND_CONTACT|");
            break;
        case FOUNDED_GROUP:
            printf("FOUNDED_GROUP|");
            break;
        case NOT_FOUND_GROUP:
            printf("NOT_FOUND_GROUP|");
            break;
        case FAVORITE:
            printf("FAVORITE|");
            break;
        case DELETE_CONTACTS_IN_FAVORITE:
            printf("DELETE_CONTACTS_IN_FAVORITE|");
            break;
        case DELETE_CONTACTS_IN_FAVORITE_RESULT:
            printf("DELETE_CONTACTS_IN_FAVORITE_RESULT|");
            break;
        case ERROR_CONTACT_NOT_EXITS_IN_FAVORITE:
            printf("ERROR_CONTACT_NOT_EXITS_IN_FAVORITE|");
            break;

        case INPUT_CONTACT_INFO:
            printf("INPUT_CONTACT_INFO|");
            break;
        case ADD_CONTACT_OK:
            printf("ADD_CONTACT_OK|");
            break;
        case ERROR_CONTACT_NAME_EXISTED:
            printf("ERROR_CONTACT_NAME_EXISTED|");
            break;
        case ADD_GROUP_OK:
            printf("ADD_GROUP_OK|");
            break;
        case ERROR_GROUP_NAME_EXISTED:
            printf("ERROR_GROUP_NAME_EXISTED|");
            break;
        case ERROR_EXISTS_IN_FAVORITE:
            printf("ERROR_EXISTS_IN_FAVORITE|");
            break;
        case ERROR_CONTACT_NOT_EXISTS:
            printf("ERROR_CONTACT_NOT_EXISTS|");
            break;
        case ADD_FAVORITE_OK:
            printf("ADD_FAVORITE_OK|");

            break;
        /* code for add contact to group */
        case CHOOSE_GROUP:
            printf("CHOOSE_GROUP|");
            break;
        case ADD_CONTACT_TO_GROUPS_RESULT:
            printf("ADD_CONTACT_TO_GROUPS_RESULT|");
            break;
        case ERROR_GROUP_NOT_EXISTS:
            printf("ERROR_GROUP_NOT_EXISTS|");
            break;
        case ERROR_EXISTS_IN_GROUP:
            printf("ERROR_EXISTS_IN_GROUP|");
            break;
        /* end code for add contact to group */

        case UPDATE_CONTACT_OK:
            printf("UPDATE_CONTACT_OK|");
            break;
        case UPDATE_GROUP_OK:
            printf("UPDATE_GROUP_OK|");
            break;
        case DELETE_GROUP_OK:
            printf("DELETE_GROUP_OK|");
            break;
        case REMOVE_CONTACTS_FROM_GROUP_RESULT:
            printf("REMOVE_CONTACTS_FROM_GROUP_RESULT|");
            break;

        case DELETE_CONTACT_OK:
            printf("DELETE_CONTACT_OK|");
            break;

        case BACKUP_OK:
            printf("BACKUP_OK|");
            break;
        case BACKUP_ERROR_CANNOT_SET_NAME_COINCIDE_WITH_FILE_DATA:
            printf("BACKUP_ERROR_CANNOT_SET_NAME_COINCIDE_WITH_FILE_DATA|");
            break;
        case BACKUP_ERROR_FILE_ALREADY_EXISTED:
            printf("BACKUP_ERROR_FILE_ALREADY_EXISTED|");
            break;
        case BACKUP_ERROR_CANNOT_MAKE_FILE:
            printf("BACKUP_ERROR_CANNOT_MAKE_FILE|");
            break;

        case RESTORE_OK:
            printf("RESTORE_OK|");
            break;
        case GET_ALL_FILES:
            printf("GET_ALL_FILES|");
            break;
        case ALL_FILES:
            printf("ALL_FILES|");
            break;
        case ERROR_NO_FILE:
            printf("ERROR_NO_FILE|");
            break;
        case FILE_NOT_EXIST:
            printf("FILE_NOT_EXIST|");
            break;
        case LOGOUT:
            printf("LOGOUT|");
            break;
        case LOGOUT_OK:
            printf("LOGOUT_OK|");
            break;
        case STATUS_FAILURE:
            printf("STATUS_FAILURE|");
            break;
        case RE_LOGIN:
            printf("RE_LOGIN|");
            break;
        case RE_LOGIN_OK:
            printf("RE_LOGIN_OK|");
            break;
            break;
        default:
            printf("UNKNOWN|");
            break;
    }
    if(strcmp(res.value, "") != 0){
        printf("%s\n", res.value);
    } else printf("\n");
}

void logStatus(ClientInfo client){
    printf("       Status: ");
    switch(client.status){
        case UNAUTHENTICATED:
            printf("UNAUTHENTICATED\n");
            break;
        case SPECIFIED_ID:
            printf("SPECIFIED_ID\n");
            break;
        case AUTHENTICATED:
            printf("AUTHENTICATED\n");
            break;
        case FORGOT_PASSWORD:
            printf("FORGOT_PASSWORD\n");
            break;
        case CHANGE_PASSWORD:
            printf("CHANGE_PASSWORD\n");
            break;
        case BLOCKING:
            printf("BLOCKING\n");
            break;
        case SIGN_USER:
            printf("SIGN_USER\n");
            break;
        case SIGN_PASS_AND_OHTER_INFO:
            printf("SIGN_PASS_AND_OHTER_INFO\n");
            break;
        default:
            printf("UNKNOWN\n");
            break;
    }
}

void resetResponse(Res* response){
    response->code = -1;
    strcpy(response->value, "");
}
