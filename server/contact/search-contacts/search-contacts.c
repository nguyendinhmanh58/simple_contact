#include "search-contacts.h"
void ProcessSearchContacts(Res request, int index,ClientInfo clients[],Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    bTreeElementtype e;
    TreeNode* contacts[100];
    int n, i;
    n = 0;
    int number_contacts = 0;
    strcpy(e.name, request.value);
    bSearchMulti(clients[index].contacts, e, contacts, &n);
    if(n!= 0){
        response->code = FOUNDED_CONTACTS;
        sprintf(response->value, "%d", n);
        for(i = 0; i < n; i++){
            strcat(response->value, "|");
            strcat(response->value, contacts[i]->element.name);
        }
        return;
    }  else {
        response->code = NOT_FOUND_CONTACT;
        sprintf(response->value, "%sNo results found%s", ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
        return;
    }
}
