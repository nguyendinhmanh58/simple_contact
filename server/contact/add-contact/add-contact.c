#include "add-contact.h"
#include "./../../libs/server_lib.h"
#include "./../../../helper/functions.h"
void ProcessAddContact(Res request, int index, ClientInfo clients[], Res *response){
    int number_groups;
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    response->code = INPUT_CONTACT_INFO;
    number_groups = numberNodeDllist(clients[index].groups);
    sprintf(response->value, "%d", number_groups);
    DllistNode* node;
    if(number_groups != 0){
        for(node = clients[index].groups->head; node != NULL; node = node->next){
            strcat(response->value, "|");
            strcat(response->value, node->element.name);
        }
    }
}

void ProcessRegisterNewContact(Res request, int index, ClientInfo clients[], Res *response){
    char fileName[100], group_names[200];
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    bTreeElementtype e;
    TreeNode* node;
    DllistElementtype* group;
    int i, number_groups;
    // *  <name>|<phone>|<email>|<address>|<group_name>|<favorite>|<note>
    strcpy(e.name, getStrByIndex(request.value, 0, "|"));
    node = bSearch(clients[index].contacts, e);
    if(node != NULL){
        response->code = ERROR_CONTACT_NAME_EXISTED;
        sprintf(response->value, "%sError. This contact name has been existed!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
    strcpy(e.phone, getStrByIndex(request.value, 1, "|"));
    strcpy(e.email, getStrByIndex(request.value, 2, "|"));
    strcpy(e.address, getStrByIndex(request.value, 3, "|"));
    e.group_names = makeDllist();
    strcpy(group_names, getStrByIndex(request.value,4, "|"));
    number_groups = atoi(getStrByIndex(group_names, 0, "-"));
    if(number_groups > 0){
        for(i = 1; i <= number_groups; i++){
            group = (DllistElementtype *)malloc(sizeof(DllistElementtype));
            strcpy(group->name, getStrByIndex(group_names, i, "-"));
            insertAtEndDllist(e.group_names, *group);
        }
    }
    e.favorite = atoi(getStrByIndex(request.value, 5, "|"));
    strcpy(e.note, getStrByIndex(request.value, 6, "|"));
    bInsert(&clients[index].contacts, e);
    // save contact info to file
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = ADD_CONTACT_OK;
    sprintf(response->value, "%sAdd contact success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}

