#ifndef ADD_CONTACT_H
#define ADD_CONTACT_H
#include "./../../../libs/typedef.h"
void ProcessAddContact(Res request, int index, ClientInfo clients[], Res *response);
void ProcessRegisterNewContact(Res request, int index, ClientInfo clients[], Res *response);
#endif // ADD_CONTACT_H
