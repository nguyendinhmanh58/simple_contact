#include "add-favorite.h"
void ProcessAddFavorite(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    bTreeElementtype e;
    TreeNode* contact;
    char fileName[100];
    // get Name of contact to add favorite
    strcpy(e.name, getStrByIndex(request.value, 0, "|"));
    contact = bSearch(clients[index].contacts, e);
    if(contact == NULL){
        response->code = ERROR_CONTACT_NOT_EXISTS;
        sprintf(response->value, "%sError. This contact do not exist!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
    if(contact->element.favorite == 1){
        response->code = ERROR_EXISTS_IN_FAVORITE;
        sprintf(response->value, "%sError. This contact has been added to your favorite!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
    contact->element.favorite = 1;
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = ADD_FAVORITE_OK;
    sprintf(response->value, "%sAdd to favorite success!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}
