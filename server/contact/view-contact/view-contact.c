#include "view-contact.h"
void ProcessViewContact(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    TreeNode* contact;
    bTreeElementtype e;
    char str[100];
    strcpy(e.name, request.value);
    contact = bSearch(clients[index].contacts, e);
    if(contact != NULL){
        response->code = FOUNDED_CONTACT;
        sprintf(response->value, "%s|%s|%s|%s",
            contact->element.name,
            contact->element.phone,
            contact->element.email,
            contact->element.address
        );
        int i, number_groups;
        DllistNode* group;
        number_groups = numberNodeDllist(contact->element.group_names);
        //
        sprintf(str, "|%d", number_groups);
        strcat(response->value, str);
        if(number_groups > 0){
            for(group = contact->element.group_names->head; group != NULL;  group = group->next){
                sprintf(str, "-%s", group->element.name);
                strcat(response->value, str);
            }
        }
        sprintf(str, "|%d|%s\n",
            contact->element.favorite,
            contact->element.note);
        strcat(response->value, str);
    } else {
        response->code = NOT_FOUND_CONTACT;
        sprintf(response->value, "%sNo results found%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
        return;
    }
}
