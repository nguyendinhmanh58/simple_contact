#include "update-contact.h"
void ProcessUpdateContact(Res request, int index, ClientInfo clients[],Res* response){
    // format request
    // <old_contact_name>|<new_contact_name>|<phone>|<email>|<address>|<number_groups>-<group_name1>-<group_name1>|<favorite>|<note>
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    bTreeElementtype be;
    DllistElementtype* group;
    DllistNode* group_node;
    TreeNode *old_contact, *new_contact;
    char str[100], group_names[200], fileName[100];
    int number_groups, i;
    // get old_contact_name
    strcpy(be.name, getStrByIndex(request.value, 0, "|"));
    old_contact = bSearch(clients[index].contacts, be);
    if(old_contact == NULL){
        response->code = ERROR_CONTACT_NOT_EXISTS;
        sprintf(str, "Contact \"%s\" do not exist!\n", be.name);
        strcpy(response->value, str);
        return;
    }
    // check new contact name is existed?
    strcpy(be.name, getStrByIndex(request.value, 1, "|"));
    new_contact = bSearch(clients[index].contacts, be);
    if(new_contact != NULL && new_contact != old_contact){
        response->code = ERROR_CONTACT_NAME_EXISTED;
        sprintf(str, "%sError. Contact \"%s\" is existed!\n%s",ANSI_COLOR_RED, be.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    // update contact
    strcpy(old_contact->element.name, getStrByIndex(request.value, 1, "|"));
    strcpy(old_contact->element.phone, getStrByIndex(request.value, 2, "|"));
    strcpy(old_contact->element.email, getStrByIndex(request.value, 3, "|"));
    strcpy(old_contact->element.address, getStrByIndex(request.value, 4, "|"));
    old_contact->element.group_names = makeDllist();
    strcpy(group_names, getStrByIndex(request.value,5, "|"));
    number_groups = atoi(getStrByIndex(group_names, 0, "-"));
    if(number_groups > 0){
        for(i = 1; i <= number_groups; i++){
            group = (DllistElementtype *)malloc(sizeof(DllistElementtype));
            strcpy(group->name, getStrByIndex(group_names, i, "-"));
            insertAtEndDllist(old_contact->element.group_names, *group);
        }
    }
    old_contact->element.favorite = atoi(getStrByIndex(request.value, 6, "|"));
    strcpy(old_contact->element.note, getStrByIndex(request.value, 7, "|"));

    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = UPDATE_CONTACT_OK;
    // return new contact info
    sprintf(response->value, "%s|%s|%s|%s",
        old_contact->element.name,
        old_contact->element.phone,
        old_contact->element.email,
        old_contact->element.address
    );
    sprintf(str, "|%d", number_groups);
    strcat(response->value, str);
    if(number_groups > 0){
        for(group_node = old_contact->element.group_names->head; group_node != NULL;  group_node = group_node->next){
            sprintf(str, "-%s", group_node->element.name);
            strcat(response->value, str);
        }
    }
    sprintf(str, "|%d|%s\n",
        old_contact->element.favorite,
        old_contact->element.note);
    strcat(response->value, str);
}
