#ifndef ADD_TO_GROUP_H
#define ADD_TO_GROUP_H
#include "./../../../libs/typedef.h"
void ProcessGetAllGroups(Res request, int index, ClientInfo clients[], Res* response);
void ProcessAddContactToGroup(Res request, int index, ClientInfo clients[], Res* response);
#endif
