#include "add-to-group.h"
void ProcessGetAllGroups(Res request, int index, ClientInfo clients[], Res* response){
    int number_groups;
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    response->code = CHOOSE_GROUP;
    number_groups = numberNodeDllist(clients[index].groups);
    sprintf(response->value, "%d", number_groups);
    DllistNode* node;
    if(number_groups != 0){
        for(node = clients[index].groups->head; node != NULL; node = node->next){
            strcat(response->value, "|");
            strcat(response->value, node->element.name);
        }
    }
}
void ProcessAddContactToGroup(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    bTreeElementtype be;
    DllistElementtype *de;
    char fileName[100], str[100];
    TreeNode* contact;
    DllistNode* group;
    int number_groups, i;
    strcpy(be.name, getStrByIndex(request.value, 0, "|"));
    contact = bSearch(clients[index].contacts, be);
    if(contact == NULL){
        response->code = ERROR_CONTACT_NOT_EXISTS;
        sprintf(response->value, "%sError. Contact \"%s\" do not exist!\n%s", ANSI_COLOR_RED, be.name, ANSI_COLOR_RESET);
        return;
    }
    number_groups = atoi(getStrByIndex(request.value, 1, "|"));
    response->code = ADD_CONTACT_TO_GROUPS_RESULT;
    if(number_groups != 0){
        for(i = 0; i < number_groups; i++){
            de = (DllistElementtype *)malloc(sizeof(DllistElementtype));
            strcpy(de->name, getStrByIndex(request.value, i+2, "|"));
            group = searchDllist(clients[index].groups, *de);
            if(group == NULL){
                sprintf(str, "%sError. Group \"%s\" do not exist!\n%s", ANSI_COLOR_RED, de->name, ANSI_COLOR_RESET);
                strcat(response->value, str);
            } else{
                // has been added to group
                group = NULL;
                group = searchDllist(contact->element.group_names, *de);
                if(group != NULL){
                    sprintf(str, "%sError. This contact has been  added to group \"%s\"!\n\t\t\t%s",ANSI_COLOR_RED, de->name, ANSI_COLOR_RESET);
                    strcat(response->value, str);
                } else{
                    // update group
                    insertAtEndDllist(contact->element.group_names, *de);
                    sprintf(str, "%sAdd to group \"%s\" success!\n\t\t\t%s",ANSI_COLOR_BLUE, de->name, ANSI_COLOR_RESET);
                    strcat(response->value, str);
                }
            }
        }
    }
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
}
