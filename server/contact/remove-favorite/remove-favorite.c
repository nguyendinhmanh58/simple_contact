#include "remove-favorite.h"
void ProcessDeleteContactsInFavorite(Res request, int index, ClientInfo clients[],Res *response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    int number_contacts, i;
    TreeNode* contact;
    bTreeElementtype *be;
    char str[100], fileName[200];
    response->code = DELETE_CONTACTS_IN_FAVORITE_RESULT;
    number_contacts = atoi(getStrByIndex(request.value, 0, "|"));
    if(number_contacts > 0){
        for(i = 1; i <= number_contacts; i++){
            be = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            strcpy(be->name, getStrByIndex(request.value, i, "|"));
            contact = bSearch(clients[index].contacts, *be);
            if(contact == NULL){
                sprintf(str,  "%sContact \"%s\" do not exist!\n\t\t\t%s",ANSI_COLOR_RED, be->name, ANSI_COLOR_RESET);
                strcat(response->value, str);
            } else {
                sprintf(str, "%sRemove contact \"%s\" from list favorite success!\n\t\t\t%s",ANSI_COLOR_BLUE, be->name, ANSI_COLOR_RESET);
                strcat(response->value, str);
                contact->element.favorite = 0;
            }
        }
    }
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
}
