#include "delete-contact.h"

void ProcessDeleteContact(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    bTreeElementtype be;
    TreeNode* node;
    char str[100], fileName[100];
    // get contact name delete
    strcpy(be.name, getStrByIndex(request.value, 0, "|"));
    node = bSearch(clients[index].contacts, be);
    if(node == NULL){
        response->code = ERROR_CONTACT_NOT_EXISTS;
        sprintf(str, "%sError. Contact \"%s\" do not exist!\n%s",ANSI_COLOR_RED, be.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    deleteBtreeNode(&clients[index].contacts, be);
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = DELETE_CONTACT_OK;
    sprintf(response->value, "%sDelete success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}

