#include "view-favorite.h"
void ProcessViewFavorite(Res request, int index, ClientInfo clients[], Res *response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    };
    TreeNode* contacts[200];
    int n, i;
    n = 0;
    bSearchFavoriteContacts(clients[index].contacts, contacts, &n);
    response->code = FAVORITE;
    sprintf(response->value, "%d", n);
    if(n > 0){
        for(i = 0; i < n; i++){
            strcat(response->value, "|");
            strcat(response->value, contacts[i]->element.name);
        }
    }
}
