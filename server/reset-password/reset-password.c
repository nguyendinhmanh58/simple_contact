#include "reset-password.h"

void ProcessResetPassword(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        sprintf(response->value, "%sError. You must be login to use this feature!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);;
        return;
    }
    char old_password[50];
    strcpy(old_password, getStrByIndex(request.value, 0, "|"));
    if(strcmp(old_password, clients[index].current_account.password) != 0){
        response->code = ERROR_RESET_PASSWORD;
        sprintf(response->value, "%sError. Old password is not match!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    } else {
        response->code = INPUT_NEW_PASS;
        clients[index].status = CHANGE_PASSWORD;
        // function change password same with change password in case forgot-password
    }
}
