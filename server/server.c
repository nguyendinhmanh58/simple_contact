#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <poll.h>
#include <time.h>
#define PORT 5554
#define BACKLOG 50
#define MAX_FD 51
#define TIMEOUT -1 /*define time logs with default value: 15 minutes = 900 seconds*/
#include "./../libs/typedef.h"
#include "./../libs/define.h"
#include "./../libs/res_code.h"
#include "./../libs/md5/md5.h"
#include "./../libs/sllib/sllib.h"
#include "./../helper/functions.h"
#include "libs/server_lib.h"
#include "login/login.h"
#include "logout/logout.h"
#include "signup/signup.h"
#include "forgot-password/forgot-password.h"
#include "contact/add-contact/add-contact.h"
#include "contact/add-favorite/add-favorite.h"
#include "contact/add-to-group/add-to-group.h"
#include "contact/delete-contact/delete-contact.h"
#include "contact/remove-favorite/remove-favorite.h"
#include "contact/update-contact/update-contact.h"
#include "contact/view-contact/view-contact.h"
#include "contact/view-favorite/view-favorite.h"
#include "contact/search-contacts/search-contacts.h"
#include "group/add-group/add-group.h"
#include "group/delete-group/delete-group.h"
#include "group/remove-contact/remove-contact.h"
#include "group/update-group/update-group.h"
#include "group/view-group/view-group.h"
#include "backup/backup.h"
#include "restore/restore.h"
// global variable

#define TRUE             1
#define FALSE            0
SecretQuestion secret_questions[NUMBER_SECRET_QUESTION];
SllistType* listAccount;

int main()
{
    int listenfd, connfd, sockfd;
    int bytes_sent, bytes_received;
    struct sockaddr_in server;      /* server's address information */
    struct sockaddr_in client_sock; /* client's address information */
    struct pollfd ufds[MAX_FD];
    //int client_status[MAX_FD];
    //int client_pass_fail_times[MAX_FD];
    ClientInfo clients[MAX_FD];
    int optval;
    // set SO_REUSEADDR on a socket to true (1):
    optval = 1;

    Res request;

    int i, rv, sin_size, close_conn;
    char *p;
    char str[1024], file_name[20], s[100];
    FILE *f;

    /* init list account*/
    elementtype *e;
    SllistNode* node;
    int total;
    init(secret_questions);

    listAccount = makeSllist();
    /* end init list account*/
    if(readAccountsInfo(listAccount, "server/account/account.txt") == 0){
        return 0;
    }
    /* init value for array ufds, client_status */

    for(i=0; i<MAX_FD; i++)
    {
        clients[i].status = -1;
        clients[i].password_fail_times = 0;
        ufds[i].fd = -1;
        //client_status[i] = -1;
    }

    /* init socket */
    if ((listenfd=socket(AF_INET, SOCK_STREAM, 0)) == -1 )   /* calls socket() */
    {
        printf("socket() error\n\n");
        return 1;
    }

    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);   /* Remember htons() from "Conversions" section? =) */
    server.sin_addr.s_addr = htonl(INADDR_ANY);  /* INADDR_ANY puts your IP address automatically */

    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

    if(bind(listenfd,(struct sockaddr*)&server,sizeof(server))==-1)  /* calls bind() */
    {
        printf("bind() error\n\n");
        return 1;
    }

    if(listen(listenfd,BACKLOG) == -1)   /* calls listen() */
    {
        printf("listen() error\n");
        return 1;
    }

    ufds[0].fd = listenfd;
    ufds[0].events = POLLIN;
    //maxfd = 0;
    printf("Server simple contact is running ...\n\n");
    while(1)
    {
        rv = poll(ufds, MAX_FD, TIMEOUT);

        if(rv == -1)
        {
            printf("poll error\n\n");
            return 1;
        }
        else if (rv == 0)
        {
            printf("Timeout occurred! No data after %d\n\n", TIMEOUT);
            return 1;
        }

        /* if has request to listen fd */
        if(ufds[0].revents & POLLIN)
        {
            if((connfd = accept(listenfd,(struct sockaddr *)&client_sock,&sin_size))==-1)  /* calls accept() */
            {
                printf("accept() error\n\n");
                return 1;
            }
            printf("Got a connection from %s\n\n",inet_ntoa(client_sock.sin_addr) ); /* prints client's IP */
            /* check and add connfd to Array ufds and init status for connfd*/
            for(i=1 ; i < MAX_FD; i++)
            {
                if(ufds[i].fd == -1)
                {
                    ufds[i].fd = connfd;
                    ufds[i].events = POLLIN|POLLOUT;
                    //client_status[i] = UNAUTHENTICATED;
                    clients[i].status = UNAUTHENTICATED;
                    break;
                }
            }
            if(i == MAX_FD)
            {
                printf("Too many clients\n\n");
                close(connfd);
            }
            else
            {
                bytes_sent = send(connfd,"Welcome to my server.\n",22,0); /* send to the client welcome message */
                if (bytes_sent < 0)
                {
                    printf("Error! Can not sent data to client!\n\n");
                    close(connfd);
                }
            }
        }

        //check the status of connfd(s)
        for(i=1; i < MAX_FD; i++)
        {
            if(ufds[i].revents & POLLIN)
            {
                //close_conn = FALSE;
                sockfd = ufds[i].fd;
                bytes_received = recv(sockfd,&request,sizeof(Res),0);
                if (bytes_received < 0)
                {
                    printf("Error!Can not receive data from client!\n\n");
                    close(ufds[i].fd);
                    ufds[i].fd = -1;
                    continue;
                }
                /*****************************************************/
                /* Check to see if the connection has been           */
                /* closed by the client                              */
                /*****************************************************/
                if (bytes_received == 0)
                {
                    printf("==> Connection socket (socket number - %d) has been closed\n\n", ufds[i].fd);
                    close(ufds[i].fd);
                    ufds[i].fd = -1;
                    continue;
                }
                printf("(%d)==> Request: ", i);
                logRes(request);
                logStatus(clients[i]);
                Res response;
                switch(request.code)
                {
                /*feature login*/
                case LOGIN_USER:
                    ProcessLoginUser(request, i, clients, listAccount, &response);
                    break;
                case LOGIN_PASS:
                    ProcessLoginPass(request, i, clients, &response, listAccount);
                    break;
                /*end feature login*/

                /*feature signup*/
                case SIGN:
                    ProcessSign(request, i, clients, &response);
                    break;
                case SIGN_USER_ID:
                    ProcessSignUserId(request, i, clients, &response, listAccount, secret_questions);
                    break;
                case SIGN_INFO:
                    ProcessSignInfo(request, i, clients, &response, listAccount);
                    break;
                /*end feature signup*/

                /*feature forgot pass*/
                case FORGOT_PASS:
                    ProcessForgotPass(request, i, clients, &response, secret_questions);
                    break;
                case ANSWER_QUESTION:
                    ProcessAnswerQuestion(request, i, clients, &response);
                    break;
                case REGISTER_NEW_PASS:
                    ProcessRegisterNewPass(request, i, clients, &response, listAccount);
                    break;
                /*end feature forgot pass*/

                /* feature contact */
                case ADD_CONTACT:
                    ProcessAddContact(request, i, clients, &response);
                    break;
                case REGISTER_NEW_CONTACT:
                    ProcessRegisterNewContact(request, i, clients, &response);
                    break;
                case SEARCH_CONTACTS:
                    ProcessSearchContacts(request, i, clients, &response);
                    break;
                case VIEW_CONTACT:
                    ProcessViewContact(request, i, clients, &response);
                    break;
                case ADD_FAVORITE:
                    ProcessAddFavorite(request, i, clients, &response);
                    break;
                /* end feature add contact to group*/
                case UPDATE_CONTACT:
                    ProcessUpdateContact(request, i, clients, &response);
                    break;
                /* feature delete contact*/
                case DELETE_CONTACT:
                    ProcessDeleteContact(request, i, clients, &response);
                    break;
                /* end feature contact*/

                /*feature group*/
                case ADD_GROUP:
                    ProcessAddGroup(request, i, clients, &response);
                    break;
                /* feature add contact to group */
                case GET_ALL_GROUPS:
                    ProcessGetAllGroups(request, i, clients, &response);
                    break;
                case ADD_CONTACT_TO_GROUP:
                    ProcessAddContactToGroup(request, i, clients, &response);
                    break;
                /* feature view group detail*/
                case VIEW_GROUP:
                    ProcessViewGroup(request, i, clients, &response);
                    break;
                /* feature update group*/
                case UPDATE_GROUP:
                    ProcessUpdateGroup(request, i, clients, &response);
                    break;
                /* feature remove contacts from groups*/
                case REMOVE_CONTACTS_FROM_GROUP:
                    ProcessRemoveContactsFromGroup(request, i, clients, &response);
                /* feature delete group*/
                    break;
                case DELETE_GROUP:
                    ProcessDeleteGroup(request, i, clients, &response);
                    break;
                /* feature backup*/
                case BACKUP:
                    ProcessBackup(request, i, clients, &response, 0);
                    break;
                case BACKUP_WITH_REPLACE_FILE_EXISTED:
                    ProcessBackup(request, i, clients, &response, 1);
                    break;

                /* feature favorite*/
                case VIEW_FAVORITE:
                    ProcessViewFavorite(request, i, clients,&response);
                    break;
                case DELETE_CONTACTS_IN_FAVORITE:
                    ProcessDeleteContactsInFavorite(request, i, clients,&response);
                    break;
                /* end feature favorite*/

                /* feature restore*/
                case GET_ALL_FILES:
                    ProcessGetAllFiles(request, i, clients,&response);
                    break;
                case RESTORE:
                    ProcessRestore(request, i, clients,&response);
                    break;
                /* end feature restore*/

                /* feature reset password*/
                case RESET_PASSWORD:
                    ProcessResetPassword(request, i, clients, &response);
                    break;

                case LOGOUT:
                    ProcessLogout(request, i, clients, &response);
                    break;

                /* reset status of client ==> UNANTHENTICATED for accept new login*/
                case RE_LOGIN:
                    ProcessReLogin(clients, i, &response);
                    break;
                }
                bytes_sent = send(ufds[i].fd, &response, sizeof(response), 0);
                if(bytes_sent < 0){
                    printf("Error!Can not receive data from client!\n\n");
                    close(ufds[i].fd);
                    ufds[i].fd = -1;
                    continue;
                }
                printf("       Response: ");
                logRes(response);
                logStatus(clients[i]);
                resetResponse(&response);
                printf("\n");
            }
        }
    }
}

