#include "logout.h"
void ProcessLogout(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        sprintf(response->value, "%sYou have not login!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    };
    clients[index].status = UNAUTHENTICATED;
    response->code = LOGOUT_OK;
    sprintf(response->value, "%sLogout success!!!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}
