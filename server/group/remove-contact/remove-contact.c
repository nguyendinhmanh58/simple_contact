#include "remove-contact.h"
void ProcessRemoveContactsFromGroup(Res request, int index, ClientInfo clients[], Res* response){
    char fileName[100], str[100];
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    DllistElementtype group_info;
    DllistNode  *group, *group_node;
    TreeNode* contact;
    bTreeElementtype *contact_info;
    int number_contacts,i;
    strcpy(group_info.name, getStrByIndex(request.value, 0, "|"));
    group = searchDllist(clients[index].groups, group_info);
    if(group == NULL){
        response->code = ERROR_GROUP_NOT_EXISTS;
        sprintf(str, "%sError. Group \"%s\" do not exist!%s",ANSI_COLOR_RED, group_info.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    response->code = REMOVE_CONTACTS_FROM_GROUP_RESULT;
    number_contacts = atoi(getStrByIndex(request.value, 1, "|"));
    if(number_contacts > 0){
        for(i = 0; i < number_contacts; i++){
            contact_info = (bTreeElementtype *)malloc(sizeof(bTreeElementtype));
            strcpy(contact_info->name, getStrByIndex(request.value, i+2, "|"));
            contact = bSearch(clients[index].contacts, *contact_info);
            if(contact != NULL){
                group_node = searchDllist(contact->element.group_names, group_info);
                if(group_node == NULL){
                    sprintf(str, "%sContact \"%s\" not in group!\n\t\t\t%s", ANSI_COLOR_RED, contact->element.name, ANSI_COLOR_RESET);
                    strcat(response->value, str);
                } else {
                    deleteNodeDllist(contact->element.group_names, group_node);
                    sprintf(str, "%sDelete contact \"%s\" from group success!\n\t\t\t%s", ANSI_COLOR_BLUE, contact->element.name, ANSI_COLOR_RESET);
                    strcat(response->value, str);
                }
            } else {
                sprintf(str, "%sContact \"%s\" do not exist!\n\t\t\t%s",ANSI_COLOR_RED, contact_info->name, ANSI_COLOR_RESET);
                strcat(response->value, str);
            }
        }
    }
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
}

