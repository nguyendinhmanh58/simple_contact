#include "add-group.h"
#include "./../../../helper/functions.h"
void ProcessAddGroup(Res request, int index, ClientInfo clients[], Res* response){
    char fileName[100];
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    DllistElementtype* de;
    DllistNode* group;
    de = (DllistElementtype *)malloc(sizeof(DllistElementtype));
    strcpy(de->name, getStrByIndex(request.value, 0, "|"));
    traverseDllist(clients[index].groups);
    group = searchDllist(clients[index].groups, *de);
    if(group != NULL){
        response->code = ERROR_CONTACT_NAME_EXISTED;
        sprintf(response->value, "%sError. This group name is existed!\n%s", ANSI_COLOR_RED, ANSI_COLOR_RESET);
        return;
    }
    strcpy(de->desc, getStrByIndex(request.value, 1, "|"));
    insertAtEndDllist(clients[index].groups, *de);
    // save contact info to file
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = ADD_GROUP_OK;
    sprintf(response->value, "%sAdd group success!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}
