#include "delete-group.h"
void ProcessDeleteGroup(Res request, int index, ClientInfo clients[], Res* response){
    char fileName[100], str[100];
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    DllistElementtype group_info;
    DllistNode *group_delete, *group_node;
    TreeNode* contacts[200];
    int n, i;
    strcpy(group_info.name, request.value);
    group_delete = searchDllist(clients[index].groups, group_info);
    if(group_delete == NULL){
        response->code = ERROR_GROUP_NOT_EXISTS;
        sprintf(str, "%sError. Group\"%s\" do not exist!\n%s", ANSI_COLOR_RED, group_info.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    n = 0;
    bSearchContactsInGroup(clients[index].contacts, request.value, contacts, &n);
    if(n >= 0){
        for(i = 0; i < n; i++){
            group_node = searchDllist(contacts[i]->element.group_names, group_info);
            if(group_node != NULL){
                deleteNodeDllist(contacts[i]->element.group_names, group_node);
            }
        }
    }
    deleteNodeDllist(clients[index].groups, group_delete);
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = DELETE_GROUP_OK;
    sprintf(response->value, "%sDelete group success!\n%s", ANSI_COLOR_BLUE, ANSI_COLOR_RESET);
}
