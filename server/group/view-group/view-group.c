#include "view-group.h"
void ProcessViewGroup(Res request, int index, ClientInfo clients[], Res *response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    char str[100];
    DllistElementtype de;
    DllistNode* group;
    TreeNode* contacts[200];
    int n, i;
    // get name of groups to search
    strcpy(de.name, request.value);
    group = searchDllist(clients[index].groups, de);
    if(group == NULL){
        response->code = NOT_FOUND_GROUP;
        sprintf(str, "%sError. Group\"%s\" do not exist!\n%s", ANSI_COLOR_RED, de.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    // return response FOUNDED_GROUP
    response->code = FOUNDED_GROUP;
    n = 0;
    bSearchContactsInGroup(clients[index].contacts, request.value, contacts, &n);
    sprintf(response->value, "%s|%s|%d", group->element.name, group->element.desc, n);
    if(n >= 0){
        for(i = 0; i < n; i++){
            strcat(response->value, "|");
            strcat(response->value, contacts[i]->element.name);
        }
    }
}
