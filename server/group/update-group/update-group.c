#include "update-group.h"
void ProcessUpdateGroup(Res request, int index, ClientInfo clients[], Res* response){
    if(clients[index].status != AUTHENTICATED){
        response->code = STATUS_FAILURE;
        strcpy(response->value, "Account status must be AUTHENTICATED");
        return;
    }
    char str[100], fileName[100];
    DllistElementtype group_info;
    DllistNode *old_group, *new_group;
    TreeNode *contacts[200];
    TreeNode *contact;
    DllistNode *group;
    int n, i;
    // get old group name
    strcpy(group_info.name, getStrByIndex(request.value, 0, "|"));
    old_group = searchDllist(clients[index].groups, group_info);
    if(old_group == NULL){
        response->code = ERROR_GROUP_NOT_EXISTS;
        sprintf(str, "%sError. Group \"%s\" do not exist!\n%s", ANSI_COLOR_RED, group_info.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    // check new group name is existed?
    strcpy(group_info.name, getStrByIndex(request.value, 1, "|"));
    new_group = searchDllist(clients[index].groups, group_info);
    if(new_group != NULL && new_group != old_group){
        response->code = ERROR_GROUP_NAME_EXISTED;
        sprintf(str, "%sError. Group \"%s\" is existed!\n%s", ANSI_COLOR_RED, group_info.name, ANSI_COLOR_RESET);
        strcpy(response->value, str);
        return;
    }
    strcpy(group_info.desc, getStrByIndex(request.value, 2, "|"));
    // update all contacts in old group
    n = 0;
    bSearchContactsInGroup(clients[index].contacts, old_group->element.name, contacts, &n);
    if(n > 0){
        for(i = 0; i < n; i++){
            group = searchDllist(contacts[i]->element.group_names, old_group->element);
            if(group != NULL) updateDllist(group, group_info);
        }
    }
    // update groups
    updateDllist(old_group, group_info);
    getLinkFileContactsData(clients[index].current_account.username, fileName);
    saveContactsInfo(clients[index], fileName);
    response->code = UPDATE_GROUP_OK;
    // return new group info
    sprintf(response->value, "%s|%s|%d", old_group->element.name, old_group->element.desc, n);
    if(n > 0){
        for(i = 0; i < n; i++){
            strcat(response->value, "|");
            strcat(response->value, contacts[i]->element.name);
        }
    }
}
